import Express from "express";
import path from "path";
import Session from "express-session";
import boot from "./boot";
import multer from "multer";
import bodyParser from "body-parser";
// import multipart from "connect-multiparty";

const app = Express();

app.use(Express.static(path.join(__dirname, "public")));
app.use(Session({
    resave: false,
    saveUninitialized: false,
    secret: "wuued",
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
// app.use(multipart());
app.use(multer({ dest: "/" }).array("file"));
app.use(Express.urlencoded({ extended: true }));
// app.use(function(req, res, next) {
//     const msgs = req.session.messages || [];
//     res.locals.messages = msgs;
//     res.locals.hasMessages = !!msgs.length;
//     next();
//     req.session.messages = [];
// });
boot(app, {});
// app.get("/", (req, res, next) => {
//     res.send("Hello world!");
// })
// app.use(function(req, res, next) {
//     res.status(404).render("404", { url: req.originalUrl });
// });
export default app;