import Mongoose from "../core/Mongoose";
import { PaginateModel, Schema } from "mongoose";
import PaginatePlugin from "mongoose-paginate-v2";
const MaterialSchema = new Schema({
    uid: {
        type: String,
        unique: true,
    },
    flag: {
        type: Array,
    },
    userid: {
        type: String,
    },
    meta: {
        type: String, // image/audio/video
    },
    metaSize: {
        type: Number,
        require: true,
    },
    uri: {
        type: String,
        require: true,
    },
    name: {
        type: String,
    },
    templates: {
        type: Array,
    },
    createTime: {
        type: Date,
    },
    isSystem: {
        type: Boolean,
        default: false,
    },
});
MaterialSchema.plugin(PaginatePlugin);
export default Mongoose.model("MaterialModel", MaterialSchema, "material");