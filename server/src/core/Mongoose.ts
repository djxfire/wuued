import Mongoose from "mongoose";

const mbdUrl = "mongodb://localhost:27017";
Mongoose.connect(mbdUrl, { user: "microVideo", pass: "65182533", dbName: "microVideo" });
Mongoose.connection.on("connected", function() {
    console.log("数据库连接成功");
});
Mongoose.connection.on("error", function(err) {
    console.log("数据库连接异常:", err);
});
Mongoose.connection.on("disconnectied", function() {
    console.log("断开数据库连接");
});
export default Mongoose;