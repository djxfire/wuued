"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Mongoose_1 = __importDefault(require("../core/Mongoose"));
const mongoose_1 = require("mongoose");
const mongoose_paginate_v2_1 = __importDefault(require("mongoose-paginate-v2"));
const MaterialSchema = new mongoose_1.Schema({
    uid: {
        type: String,
        unique: true,
    },
    flag: {
        type: Array,
    },
    userid: {
        type: String,
    },
    meta: {
        type: String, // image/audio/video
    },
    metaSize: {
        type: Number,
        require: true,
    },
    uri: {
        type: String,
        require: true,
    },
    name: {
        type: String,
    },
    templates: {
        type: Array,
    },
    createTime: {
        type: Date,
    },
    isSystem: {
        type: Boolean,
        default: false,
    },
});
MaterialSchema.plugin(mongoose_paginate_v2_1.default);
exports.default = Mongoose_1.default.model("MaterialModel", MaterialSchema, "material");
