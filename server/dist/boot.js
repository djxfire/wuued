"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
const glob_1 = __importDefault(require("glob"));
const superagent_charset_1 = __importDefault(require("superagent-charset"));
const superagent_1 = __importDefault(require("superagent"));
function globSync(dir) {
    return new Promise((resolve, reject) => {
        (0, glob_1.default)(dir, function (er, files) {
            if (er) {
                reject(er);
            }
            else {
                resolve(files);
            }
        });
    });
}
function default_1(app, options) {
    return __awaiter(this, void 0, void 0, function* () {
        const files = yield globSync(path_1.default.resolve(__dirname, "./conf/*.json"));
        const confObjs = {};
        files.forEach((vo) => {
            const confObj = require(vo);
            Object.assign(confObjs, confObj);
        });
        app.use(function (req, res, next) {
            let pathArrs = req.path.split("/").slice(1);
            const pathKey = `/${pathArrs[0]}`;
            console.log("path obj==>", confObjs[pathKey]);
            if (confObjs[pathKey]) {
                const confObj = confObjs[pathKey];
                if (confObj.overwrite) {
                    pathArrs = pathArrs.slice(1);
                }
                const request = (0, superagent_charset_1.default)(superagent_1.default);
                const url = confObj.proxy + "/" + pathArrs.join("/");
                const agentReq = request.get(url)
                    .query(req.query)
                    .set("Accept", "*/*")
                    .set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.104 Safari/537.36 Core/1.53.3538.400 QQBrowser/9.7.12954.400")
                    .set("Accept-Language", "zh-CN");
                if (confObj.headers) {
                    for (let key in confObj.headers) {
                        agentReq.set(key, confObj.headers[key]);
                    }
                }
                agentReq.buffer(true)
                    .charset("utf-8")
                    .end((err, response) => {
                    if (err) {
                        console.log("agent error", err);
                    }
                    else {
                        // if (response.header) {
                        //     for (let key in response.header) {
                        //         res.set(key, response.header[key]);
                        //     }
                        // }
                        console.log("response==>", response);
                        res.send(response.text);
                    }
                });
            }
            else {
                next();
            }
        });
        const dir = path_1.default.join(__dirname, "controllers");
        fs_1.default.readdirSync(dir).forEach((name) => {
            const file = path_1.default.join(dir, name);
            if (fs_1.default.statSync(file).isDirectory())
                return;
            const obj = require(file);
            const fileName = obj.name || name;
            const prefix = obj.prefix || "";
            let handler;
            let method;
            let url;
            // if (obj.engine) app.set("view engine", obj.engine);
            // app.set("views", path.join(__dirname, "controllers", name, "views"));
            for (let key in obj) {
                if (["get", "put", "post", "delete"].includes(key)) {
                    method = key;
                    const handlers = obj[key];
                    for (let name in handlers) {
                        handler = handlers[name];
                        url = `${prefix}/${name}`;
                        console.log("url===>", url);
                        app[method](url, handler);
                    }
                }
            }
        });
    });
}
exports.default = default_1;
