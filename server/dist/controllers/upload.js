"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.post = exports.get = exports.engine = exports.prefix = void 0;
const Material_1 = __importDefault(require("../models/Material"));
const qiNiuUpload_1 = require("../utils/qiNiuUpload");
const uuid_1 = require("uuid");
// import { File } from "multer";
const _qiNiu_uri = 'http://assert.auage.cc/';
exports.prefix = "/upload";
exports.engine = "pug";
exports.get = {
    list(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const { flag, page = 1, pageSize = 20, meta = "image" } = req.params;
            const filter = {
                meta,
                sort: { createTime: -1 },
                page: page,
                limit: pageSize,
            };
            if (flag) {
                filter["flag"] = {
                    $elemMatch: { $regex: new RegExp(`[\S]*${flag}[\S]*`) }
                };
            }
            console.log("filter==>", filter);
            const query = Material_1.default.find();
            const docs = yield query.exec();
            res.send(JSON.stringify({
                code: 200,
                data: docs,
            }));
        });
    },
    getToken(req, res) {
        res.send(JSON.stringify({
            code: 200,
            data: (0, qiNiuUpload_1.getToken)(),
        }));
    }
};
exports.post = {
    add(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const { userid, meta = "image", flag, files = [], isSystem = false } = req.body;
            // const files = req.files as File[];
            // const readableStreams = files.map((file: any) => ({
            //     file: fs.createReadStream(file.path),
            //     ext: file.filename.substr(file.originalname.lastIndexOf('.') + 1),
            //     filename: file.originalname,
            // }));
            // const uploadInfo = await uploadFiles(readableStreams);
            const models = files.map((vo) => ({
                uid: (0, uuid_1.v4)(),
                flag: !!flag ? flag.split(",") : [],
                meta,
                userid,
                name: vo.filename,
                metaSize: vo.fsize,
                isSystem: isSystem,
                uri: vo.path,
                createTime: new Date(),
            }));
            console.log("modelsss=>", models, req.body);
            const result = yield Material_1.default.insertMany(models);
            res.send({
                code: 200,
                data: result,
            });
        });
    }
};
