"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const path_1 = __importDefault(require("path"));
const express_session_1 = __importDefault(require("express-session"));
const boot_1 = __importDefault(require("./boot"));
const multer_1 = __importDefault(require("multer"));
const body_parser_1 = __importDefault(require("body-parser"));
// import multipart from "connect-multiparty";
const app = (0, express_1.default)();
app.use(express_1.default.static(path_1.default.join(__dirname, "public")));
app.use((0, express_session_1.default)({
    resave: false,
    saveUninitialized: false,
    secret: "wuued",
}));
app.use(body_parser_1.default.json());
app.use(body_parser_1.default.urlencoded({ extended: true }));
// app.use(multipart());
app.use((0, multer_1.default)({ dest: "/" }).array("file"));
app.use(express_1.default.urlencoded({ extended: true }));
// app.use(function(req, res, next) {
//     const msgs = req.session.messages || [];
//     res.locals.messages = msgs;
//     res.locals.hasMessages = !!msgs.length;
//     next();
//     req.session.messages = [];
// });
(0, boot_1.default)(app, {});
// app.get("/", (req, res, next) => {
//     res.send("Hello world!");
// })
// app.use(function(req, res, next) {
//     res.status(404).render("404", { url: req.originalUrl });
// });
exports.default = app;
