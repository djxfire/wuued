"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const mbdUrl = "mongodb://localhost:27017";
mongoose_1.default.connect(mbdUrl, { user: "microVideo", pass: "65182533", dbName: "microVideo" });
mongoose_1.default.connection.on("connected", function () {
    console.log("数据库连接成功");
});
mongoose_1.default.connection.on("error", function (err) {
    console.log("数据库连接异常:", err);
});
mongoose_1.default.connection.on("disconnectied", function () {
    console.log("断开数据库连接");
});
exports.default = mongoose_1.default;
