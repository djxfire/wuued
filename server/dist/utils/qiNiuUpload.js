"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteFile = exports.uploadFiles = exports.uploadFile = exports.getToken = void 0;
const qiniu_1 = __importDefault(require("qiniu"));
const accessKey = 'JUamKcvBktrWmDbq-CCxfLYxbCOqAjdIciKaYbUc';
const secretKey = 'ZJM4NzyIHB07-hWhvp8VeU8wyaL75Xg9QGdcBW7w';
const mac = new qiniu_1.default.auth.digest.Mac(accessKey, secretKey);
const bucket = 'micr0video';
const options = {
    scope: bucket,
    returnBody: '{"host":"http://assert.auage.cc/","key":"$(key)","hash":"$(etag)","fsize":$(fsize),"bucket":"$(bucket)","name":"$(x:name)","age":$(x:age)}',
};
const putPolicy = new qiniu_1.default.rs.PutPolicy(options);
const uploadToken = putPolicy.uploadToken(mac);
const config = new qiniu_1.default.conf.Config({
    zone: qiniu_1.default.zone.Zone_z0,
});
function getToken() {
    const token = putPolicy.uploadToken(mac);
    return token;
}
exports.getToken = getToken;
function uploadFile(readableStream) {
    const formUploader = new qiniu_1.default.form_up.FormUploader(config);
    const putExtra = new qiniu_1.default.form_up.PutExtra();
    const key = new Date().getTime() + (Math.random() * 10).toFixed(0);
    return new Promise((resolve, reject) => {
        formUploader.putStream(uploadToken, key, readableStream, putExtra, (respErr, respBody, respInfo) => {
            if (respErr) {
                reject(respErr);
                return;
            }
            if (respInfo.statusCode === 200) {
                resolve(respInfo);
            }
        });
    });
}
exports.uploadFile = uploadFile;
function uploadFiles(readableStreams) {
    const formUploader = new qiniu_1.default.form_up.FormUploader(config);
    const putExtra = new qiniu_1.default.form_up.PutExtra();
    return Promise.all(readableStreams.map((readableStream) => (new Promise((resolve, reject) => {
        const key = new Date().getTime() + (Math.random() * 10).toFixed(0) + '.' + readableStream.ext;
        formUploader.putStream(uploadToken, key, readableStream.file, putExtra, (respErr, respBody, respInfo) => {
            if (respErr) {
                reject(respErr);
                return;
            }
            if (respInfo.statusCode === 200) {
                resolve(Object.assign(Object.assign({}, respInfo.data), { filename: readableStream.filename }));
            }
            else {
                reject('上传失败');
            }
        });
    }))));
}
exports.uploadFiles = uploadFiles;
function deleteFile(key) {
    const bucketManager = new qiniu_1.default.rs.BucketManager(mac, config);
    return new Promise((resolve, reject) => {
        bucketManager.delete(bucket, key, (err, respBody, respInfo) => {
            if (err) {
                console.log('delete err===>', err);
                reject(err);
            }
            else {
                if (respInfo.statusCode === 200) {
                    resolve('删除成功');
                }
                else {
                    reject('删除失败');
                }
            }
        });
    });
}
exports.deleteFile = deleteFile;
