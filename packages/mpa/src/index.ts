import type { Plugin, ResolvedConfig, ViteDevServer, UserConfig } from "vite";
import type { UserOptions } from "./lib/options";
import path from "path";
import shell from "shelljs";
import {
  getMPAIO,
  getHtmlContent,
  getFirstPage,
  getPagesInfo,
  // getHistoryReWriteRuleList,
} from "./lib/utils";
import { name } from "../package.json";
// import history from "connect-history-api-fallback";

const resolve = (p: string) => path.resolve(process.cwd(), p);

export default function mpa(userOptions: UserOptions = {}): Plugin {
  let config: ResolvedConfig;
  const options = {
    open: "",
    scanDir: "src/pages",
    scanFile: "main.{js,ts,jsx,tsx}",
    defaultEntries: "",
    filename: "index.html",
    rewrites: [],
    template: {},
    defaultTemplate: "public/index.html",
    ...userOptions,
  };
  if (!options.scanFile.includes(".")) {
    console.error(
      `[${name}]: scanFile should be something like main.ts/main.{js,ts}/index.js/index{ts,tsx}`
    );
    process.exit(1);
  }
  let resolvedConfig: UserConfig;
  return {
    name,
    config(config) {
      resolvedConfig = config;
      config.build = config.build || {};
      config.build.rollupOptions = config.build.rollupOptions || {};
      config.build.rollupOptions.input = getMPAIO(
        config.root || process.cwd(),
        options
      );
      console.log("output===>", config.build.rollupOptions.output);
      if (config.build.rollupOptions.output) {
        (config.build.rollupOptions.output as any).assetFileNames =
          "_pacui/assets/[name]-[hash][extname]";
        (config.build.rollupOptions.output as any).chunkFileNames =
          "_pacui/assets/[name]-[hash].js";
        (config.build.rollupOptions.output as any).entryFileNames =
          "_pacui/assets/[name]-[hash].js";
      } else {
        config.build.rollupOptions.output = {
          assetFileNames: "_pacui/assets/[name]-[hash][extname]",
          chunkFileNames: "_pacui/assets/[name]-[hash].js",
          entryFileNames: "_pacui/assets/[name]-[hash].js",
        };
      }

      config.server = config.server || {};
      // default '' means first-page and you can customized or disabled.
      config.server.open =
        options.open === ""
          ? getFirstPage(config.build.rollupOptions.input)
          : options.open;
    },
    configResolved(resolvedConfig) {
      config = resolvedConfig;
    },
    resolveId(id) {
      if (id.endsWith("html")) {
        return id;
      }
      return null;
    },
    load(id) {
      if (id.endsWith(".html") || id.endsWith("/")) {
        const { scanDir } = options;
        const pageName =
          id.match(new RegExp(`${scanDir}/(.*)/`))?.[1] || "index";
        const pages = getPagesInfo(options);
        const page = pages[pageName];
        const templateOption = options.template?.[pageName];
        const isString = (x: any): x is string => typeof x === "string";
        const templatePath = templateOption
          ? resolve(
              isString(templateOption)
                ? templateOption
                : templateOption.path || options.defaultTemplate
            )
          : resolve(options.defaultTemplate);
        const scanDirIndex = id.indexOf(scanDir);
        const idNoPrefix = id.slice(scanDirIndex + scanDir.length);
        return getHtmlContent({
          templatePath,
          entry: page.entry,
          title: isString(templateOption) ? "" : templateOption?.title || "",
          css: isString(templateOption) ? [] : templateOption?.css || [],
          script: isString(templateOption) ? [] : templateOption?.script || [],
          extraData: {
            base: config.base,
            url: idNoPrefix,
          },
        });
      }
      return null;
    },
    configureServer(server: ViteDevServer) {
      return () => {
        server.middlewares.use(async (req, res, next) => {
          console.log(!req.url?.endsWith(".html") && req.url !== "/");
          if (!req.url?.endsWith(".html") && req.url !== "/") {
            return next();
          }
          const pages = getPagesInfo(options);
          let url = req.url;
          const originalUrl = req.originalUrl;
          console.log("origin url===>", originalUrl);
          const pageName = (() => {
            if (originalUrl === "/") {
              return "index";
            }
            return originalUrl?.split("/")?.[1] || "index";
          })();
          console.log("page name===>", pageName);
          const templateOption = options.template?.[pageName];
          const isString = (x: any): x is string => typeof x === "string";
          const templatePath = templateOption
            ? resolve(
                isString(templateOption)
                  ? templateOption
                  : templateOption.path || options.defaultTemplate
              )
            : resolve(options.defaultTemplate);
          const page = pages[pageName];
          let content = await getHtmlContent({
            templatePath,
            entry: page.entry,
            title: isString(templateOption) ? "" : templateOption?.title || "",
            css: isString(templateOption) ? [] : templateOption?.css || [],
            script: isString(templateOption)
              ? []
              : templateOption?.script || [],
            extraData: {
              base: config.base,
              url,
            },
          });
          content = await server?.transformIndexHtml?.(
            url,
            content,
            req.originalUrl
          );
          res.end(content);
        });
      };

      // app.use(
      //   // @see https://github.com/vitejs/vite/blob/8733a83d291677b9aff9d7d78797ebb44196596e/packages/vite/src/node/server/index.ts#L433
      //   // @ts-ignore
      //   history({
      //     verbose: Boolean(process.env.DEBUG) && process.env.DEBUG !== "false",
      //     disableDotRule: undefined,
      //     htmlAcceptHeaders: ["text/html", "application/xhtml+xml"],
      //     rewrites: getHistoryReWriteRuleList(options),
      //   })
      // );
    },
    closeBundle() {
      const root = resolvedConfig.root || process.cwd();
      const dest =
        (resolvedConfig.build && resolvedConfig.build.outDir) || "dist";
      const resolve = (p: string) => path.resolve(root, p);
      // 1. rename all xxx.html to index.html if needed
      if (options.filename !== "index.html") {
        shell
          .ls(resolve(`${dest}/${options.scanDir}/**/*.html`))
          .forEach((html) => {
            console.log("html===>", html);
            shell.mv(html, html.replace(options.filename, "index.html"));
          });
      }
      // 2. remove all *.html at dest root
      shell.rm("-rf", resolve(`${dest}/*.html`));
      // 3. move src/pages/* to dest root
      shell.mv(resolve(`${dest}/${options.scanDir}/*`), resolve(dest));
      // 4. remove empty src dir
      shell.rm("-rf", resolve(`${dest}/src`));
    },
  };
}

export type { UserOptions as MpaOptions };
