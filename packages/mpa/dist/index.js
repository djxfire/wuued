"use strict";
var __create = Object.create;
var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __getProtoOf = Object.getPrototypeOf;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __export = (target, all) => {
  for (var name2 in all)
    __defProp(target, name2, { get: all[name2], enumerable: true });
};
var __copyProps = (to, from, except, desc) => {
  if (from && typeof from === "object" || typeof from === "function") {
    for (let key of __getOwnPropNames(from))
      if (!__hasOwnProp.call(to, key) && key !== except)
        __defProp(to, key, { get: () => from[key], enumerable: !(desc = __getOwnPropDesc(from, key)) || desc.enumerable });
  }
  return to;
};
var __toESM = (mod, isNodeMode, target) => (target = mod != null ? __create(__getProtoOf(mod)) : {}, __copyProps(
  isNodeMode || !mod || !mod.__esModule ? __defProp(target, "default", { value: mod, enumerable: true }) : target,
  mod
));
var __toCommonJS = (mod) => __copyProps(__defProp({}, "__esModule", { value: true }), mod);

// src/index.ts
var src_exports = {};
__export(src_exports, {
  default: () => mpa
});
module.exports = __toCommonJS(src_exports);
var import_path2 = __toESM(require("path"));
var import_shelljs = __toESM(require("shelljs"));

// src/lib/utils.ts
var import_fast_glob = __toESM(require("fast-glob"));
var import_yargs = __toESM(require("yargs"));
var import_path = __toESM(require("path"));
var import_fs = require("fs");
var import_lodash = require("lodash");
var argv = import_yargs.default.argv;
function getFirstPage(pages) {
  const firstPageName = Object.keys(pages)[0];
  return `/${firstPageName}/index.html`;
}
function genFileName(pageName, path3) {
  const xPath = path3 === "" ? "" : `${path3}/`;
  return `${xPath}${pageName}.html`.replace(/^pages\//, "");
}
function parseEntryFile(file, filters = []) {
  const fileSplit = file.split("/");
  const pageName = fileSplit.slice(2, -1).join("/");
  const outputPath = fileSplit.slice(1, fileSplit.length - 2).join("/");
  const result = {
    file,
    pageName,
    outputPath,
    include: filters.includes(pageName) || filters.length === 0
  };
  return result;
}
function parseFiles(files, defaultEntries) {
  const args = argv.entry || argv.file || argv.page || "";
  if (args === "") {
    defaultEntries = "";
  }
  const filters = args.split(",").concat(defaultEntries.split(",")).filter((_) => _);
  const ret = files.map((file) => parseEntryFile(file, filters));
  return {
    allEntries: ret,
    entries: ret.filter((e) => e.include),
    args
  };
}
function scanFile2Html(current, scanFile, filename) {
  const reStr = `${scanFile.split(".")[0]}[.](.*)`;
  const entryRe = new RegExp(reStr);
  return current.replace(entryRe, filename);
}
function getPagesInfo({
  defaultEntries,
  scanDir,
  scanFile
}) {
  const allFiles = import_fast_glob.default.sync(`${scanDir}/**/${scanFile}`.replace("//", "/"));
  const pages = {};
  const result = parseFiles(allFiles, defaultEntries);
  const { entries } = result;
  entries.forEach((entry) => {
    const { file, pageName, outputPath } = entry;
    pages[pageName] = {
      entry: file,
      filename: genFileName(pageName, outputPath),
      pageName,
      outputPath
    };
  });
  return pages;
}
function getMPAIO(root, options) {
  const { scanFile, filename } = options;
  console.log("get MPAIO ==>", options);
  const pages = getPagesInfo(options);
  const input = {};
  Object.keys(pages).map((key) => {
    input[key] = import_path.default.resolve(
      root,
      scanFile2Html(pages[key].entry, scanFile, filename)
    );
  });
  console.log("input==>", input, pages);
  return input;
}
async function getHtmlContent(payload) {
  const { entry, templatePath, title, css, script, description, extraData } = payload;
  let content = "";
  try {
    content = await import_fs.promises.readFile(templatePath, { encoding: "utf-8" });
  } catch (e) {
    console.error(e);
  }
  content = content.replace(
    "</body>",
    `</body>
<script type="module" src="/${entry}"><\/script>
`
  );
  const compiled = (0, import_lodash.template)(content);
  const context = {
    htmlWebpackPlugin: {
      options: {
        title
      },
      tags: {
        headTags: [],
        bodyTags: []
      },
      files: {
        publicPath: extraData.base,
        js: script,
        css,
        manifest: "",
        favicon: ""
      }
    },
    webpackConfig: {
      name: title,
      output: {
        publicPath: extraData.base
      }
    },
    title,
    BASE_URL: extraData.base,
    ...process.env
  };
  const html = compiled({
    ...context
  });
  return html;
}

// package.json
var name = "@wuued/mpa";

// src/index.ts
var resolve = (p) => import_path2.default.resolve(process.cwd(), p);
function mpa(userOptions = {}) {
  let config;
  const options = {
    open: "",
    scanDir: "src/pages",
    scanFile: "main.{js,ts,jsx,tsx}",
    defaultEntries: "",
    filename: "index.html",
    rewrites: [],
    template: {},
    defaultTemplate: "public/index.html",
    ...userOptions
  };
  if (!options.scanFile.includes(".")) {
    console.error(
      `[${name}]: scanFile should be something like main.ts/main.{js,ts}/index.js/index{ts,tsx}`
    );
    process.exit(1);
  }
  let resolvedConfig;
  return {
    name,
    config(config2) {
      resolvedConfig = config2;
      config2.build = config2.build || {};
      config2.build.rollupOptions = config2.build.rollupOptions || {};
      config2.build.rollupOptions.input = getMPAIO(
        config2.root || process.cwd(),
        options
      );
      console.log("output===>", config2.build.rollupOptions.output);
      if (config2.build.rollupOptions.output) {
        config2.build.rollupOptions.output.assetFileNames = "_pacui/assets/[name]-[hash][extname]";
        config2.build.rollupOptions.output.chunkFileNames = "_pacui/assets/[name]-[hash].js";
        config2.build.rollupOptions.output.entryFileNames = "_pacui/assets/[name]-[hash].js";
      } else {
        config2.build.rollupOptions.output = {
          assetFileNames: "_pacui/assets/[name]-[hash][extname]",
          chunkFileNames: "_pacui/assets/[name]-[hash].js",
          entryFileNames: "_pacui/assets/[name]-[hash].js"
        };
      }
      config2.server = config2.server || {};
      config2.server.open = options.open === "" ? getFirstPage(config2.build.rollupOptions.input) : options.open;
    },
    configResolved(resolvedConfig2) {
      config = resolvedConfig2;
    },
    resolveId(id) {
      if (id.endsWith("html")) {
        return id;
      }
      return null;
    },
    load(id) {
      if (id.endsWith(".html") || id.endsWith("/")) {
        const { scanDir } = options;
        const pageName = id.match(new RegExp(`${scanDir}/(.*)/`))?.[1] || "index";
        const pages = getPagesInfo(options);
        const page = pages[pageName];
        const templateOption = options.template?.[pageName];
        const isString = (x) => typeof x === "string";
        const templatePath = templateOption ? resolve(
          isString(templateOption) ? templateOption : templateOption.path || options.defaultTemplate
        ) : resolve(options.defaultTemplate);
        const scanDirIndex = id.indexOf(scanDir);
        const idNoPrefix = id.slice(scanDirIndex + scanDir.length);
        return getHtmlContent({
          templatePath,
          entry: page.entry,
          title: isString(templateOption) ? "" : templateOption?.title || "",
          css: isString(templateOption) ? [] : templateOption?.css || [],
          script: isString(templateOption) ? [] : templateOption?.script || [],
          extraData: {
            base: config.base,
            url: idNoPrefix
          }
        });
      }
      return null;
    },
    configureServer(server) {
      return () => {
        server.middlewares.use(async (req, res, next) => {
          console.log(!req.url?.endsWith(".html") && req.url !== "/");
          if (!req.url?.endsWith(".html") && req.url !== "/") {
            return next();
          }
          const pages = getPagesInfo(options);
          let url = req.url;
          const originalUrl = req.originalUrl;
          console.log("origin url===>", originalUrl);
          const pageName = (() => {
            if (originalUrl === "/") {
              return "index";
            }
            return originalUrl?.split("/")?.[1] || "index";
          })();
          console.log("page name===>", pageName);
          const templateOption = options.template?.[pageName];
          const isString = (x) => typeof x === "string";
          const templatePath = templateOption ? resolve(
            isString(templateOption) ? templateOption : templateOption.path || options.defaultTemplate
          ) : resolve(options.defaultTemplate);
          const page = pages[pageName];
          let content = await getHtmlContent({
            templatePath,
            entry: page.entry,
            title: isString(templateOption) ? "" : templateOption?.title || "",
            css: isString(templateOption) ? [] : templateOption?.css || [],
            script: isString(templateOption) ? [] : templateOption?.script || [],
            extraData: {
              base: config.base,
              url
            }
          });
          content = await server?.transformIndexHtml?.(
            url,
            content,
            req.originalUrl
          );
          res.end(content);
        });
      };
    },
    closeBundle() {
      const root = resolvedConfig.root || process.cwd();
      const dest = resolvedConfig.build && resolvedConfig.build.outDir || "dist";
      const resolve2 = (p) => import_path2.default.resolve(root, p);
      if (options.filename !== "index.html") {
        import_shelljs.default.ls(resolve2(`${dest}/${options.scanDir}/**/*.html`)).forEach((html) => {
          console.log("html===>", html);
          import_shelljs.default.mv(html, html.replace(options.filename, "index.html"));
        });
      }
      import_shelljs.default.rm("-rf", resolve2(`${dest}/*.html`));
      import_shelljs.default.mv(resolve2(`${dest}/${options.scanDir}/*`), resolve2(dest));
      import_shelljs.default.rm("-rf", resolve2(`${dest}/src`));
    }
  };
}
// Annotate the CommonJS export names for ESM import in node:
0 && (module.exports = {});
