import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import mpa from "@wuued/mpa";
import path from "path";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    mpa.default({
      scanDir: "src/pages",
    }),
    vue()
  ],
  resolve: {
    alias: {
      "@": path.resolve(__dirname, "scr/pages"),
      "~": path.resolve(__dirname, "src/"),
    },
  },
  css: {
    preprocessorOptions: {
      less: {
        javascriptEnabled: true,
        modifyVars: {
          hack: `true;@import (reference) "${path.resolve(
            __dirname,
            "src/style/index.less"
          )}";`,
        },
      },
    },
  },
})
