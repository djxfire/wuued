import App from "./App.vue";
import Application from "~/core/App";
import "~/assets/font/iconfont.js";
import "~/assets/font/iconfont.css";

new Application("mockup").start(App, "#app");