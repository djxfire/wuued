import { ACESFilmicToneMapping, Color, DirectionalLight, GridHelper, Group, Object3D, PerspectiveCamera, Scene, sRGBEncoding, WebGLRenderer } from "three";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";
import { World } from "cannon-es";
export interface D3Option {
    width?: number;
    height?: number;
    isShowGrid?: boolean;
}

export default class D3 {
    private container: HTMLElement;
    private renderer: WebGLRenderer;
    private width: number;
    private height: number;
    private scene: Scene;
    private camera: PerspectiveCamera;
    private control: OrbitControls;
    private sunLight: DirectionalLight;
    private animateFlag: number = -1;
    private clothes: Group = new Group();
    private world: World | undefined;
    constructor(ele: HTMLElement | string, option: D3Option) {
        if (typeof ele === "string") {
            this.container = document.querySelector(ele) as HTMLElement;
        } else {
            this.container = ele;
        }
        let { width, height } = this.container.getBoundingClientRect();
        if (option.width) {
            width = option.width;
        }
        if (option.height) {
            height = option.height;
        }
        this.width = width;
        this.height = height;
        this.renderer = new WebGLRenderer({
            antialias: true,
        });
        this.initRenderer();
        this.scene = new Scene();
        this.initScene();
        this.camera = new PerspectiveCamera(30, width / height, 1, 1000);
        this.initCamera();
        this.control = new OrbitControls(this.camera, this.renderer.domElement);
        this.initControl();
        this.sunLight = new DirectionalLight(0xffffff, 1.75);
        this.initLight();
        if (option.isShowGrid) {
            this.initGrid();
        }
        this.scene.add(this.clothes);
    }
    private initGrid() {
        const grid = new GridHelper(120, 50, 0xffffff);
        grid.position.y = -25;
        this.scene.add(grid);
    }
    public setWorld(world: World) {
        this.world = world;
    }
    private initRenderer() {
        this.renderer.physicallyCorrectLights = true;
        this.renderer.toneMapping = ACESFilmicToneMapping;
        this.renderer.setPixelRatio(window.devicePixelRatio);
        this.renderer.setSize(this.width, this.height);
        this.renderer.outputEncoding = sRGBEncoding;
        this.container.appendChild(this.renderer.domElement);
    }

    private initCamera() {
        this.camera.position.set(0, 80, 120);
        this.scene.add(this.camera);
    }

    private initScene() {
        this.scene.background = new Color(0x02020F);
    }

    private initLight() {
        this.sunLight.position.set(-80, 80, 120);
        this.scene.add(this.sunLight);
    }

    private initControl() {
        this.control.minDistance = 50;
        this.control.maxDistance = 300;
    }

    public render(t: number = 0) {
        this.animateFlag = requestAnimationFrame((t) => this.render(t));
        for (let i = 0; i < this.clothes.children.length; i++) {
            (this.clothes.children[i] as any).update();
        }
        this.control.update();
        if (this.world) {
            this.world.step(1 / 30);
        }
        this.renderer.render(this.scene, this.camera);
    }

    public addShape() {
        
    }

    public add(obj: Object3D) {
        this.clothes.add(obj);
    }

    public dispose() {
        if (this.animateFlag) {
            cancelAnimationFrame(this.animateFlag);
            this.animateFlag = -1;
        }
    }
}