export interface Point {
    x: number;
    y: number;
}

export interface SvgOpt {
    id: string;
    path: string;
    mtd: "M" | "L" | "V" | "H" | "C" | "S" | "Q" | "T" | "A1" | "Z" | "A0";
    done: boolean;
    next?: SvgOpt | null;
    prev?: SvgOpt | null;
    x: number;
    y: number;
    x1?: number;
    y1?: number;
    x2?: number;
    y2?: number;
    rx?: number;
    ry?: number;

}
export interface Path {
    id: string;
    opts: SvgOpt[];
}