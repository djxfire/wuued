import { Path } from "~/core/Data";
import ShapeGeometry from "~/geometry/ShapeGeometry";
import { ShapeUtils, Mesh, MeshNormalMaterial, DoubleSide, Object3D, Vector3 } from "three";
import { World, Particle, Vec3, Body, DistanceConstraint, LockConstraint } from "cannon-es";
interface ClothOption {
    widthSegments: number;
    heightSegments: number;
    curveSegments: number;
    mass: number;
}
export default class Cloth extends Object3D {
    private geo: ShapeGeometry;
    private particles: Body[];
    private world: World;
    private mesh: Mesh;
    constructor(path: Path, world: World, option: ClothOption) {
        super();
        this.geo = new ShapeGeometry(path, option);
        this.mesh = new Mesh(this.geo, new MeshNormalMaterial({
            wireframe: true,
            side: DoubleSide,
        }));
        this.add(this.mesh);
        this.scale.set(0.2, 0.2, 0.2);
        this.world = world;
        this.particles = [];
        this.initParticles(option);
    }

    public update() {
        const positionAttribute = this.geo.attributes.position;
        // console.log("particles===>", this.particles);
        for (let i = 0; i < this.particles.length; i++) {
            const particle = this.particles[i];
            const position = particle.position;
            positionAttribute.setXYZ(i, position.x, position.y, position.z);
            positionAttribute.needsUpdate = true;
        }
        
    }

    private initParticles(option: ClothOption) {
        const positionAttributes = this.geo.attributes.position.array;
        
        for (let i = 0; i < positionAttributes.length; i += 3) {
            const shape = new Particle();
            const particle = new Body({
                mass: i < (option.widthSegments - 1) * 3 ? 0 : option.mass,
                shape: shape,
                position: new Vec3(positionAttributes[i], positionAttributes[i + 1], positionAttributes[i + 2]),
                // velocity: new Vec3(0, 0, -0.1),
            });
            this.particles.push(particle);
            
            this.world.addBody(particle);
        }
        const { widthSegments, heightSegments } = option;
        const { points } = this.geo;
        for (let i = 0; i < heightSegments - 1; i++) {
            for (let j = 0; j < widthSegments - 1; j++) {
                if (i !== 0 && points[i][j].isIn) {
                    console.log("particle==>", this.particles[points[i][j].index].position);
                    this.particles[points[i][j].index].mass = 0;
                    // this.particles[points[i][j].index].aabbNeedsUpdate = true;
                }
                // if (points[i][j].isIn) {
                //     this.world.addBody()
                // }
                if (points[i][j].isIn && points[i + 1][j].isIn) {
                    this.world.addConstraint(
                        new DistanceConstraint(
                            this.particles[points[i][j].index],
                            this.particles[points[i + 1][j].index],
                            // this.geo.yStep,
                        ));
                }
                if (points[i][j].isIn && points[i][j + 1].isIn) {
                    this.world.addConstraint(
                        new DistanceConstraint(
                            this.particles[points[i][j].index],
                            this.particles[points[i][j + 1].index],
                            // this.geo.xStep,
                        )
                    );
                }
            }
        }
        for (let i = 0; i < heightSegments - 1; i++) {
            if (points[i][widthSegments - 1].isIn && points[i + 1][widthSegments - 1].isIn) {
                this.world.addConstraint(
                    new DistanceConstraint(
                        this.particles[points[i][widthSegments - 1].index],
                        this.particles[points[i + 1][widthSegments - 1].index],
                        // this.geo.xStep,
                    )
                );
            }
        }
        // if (this.particles.length) {
        //     this.particles[0].mass = 0;
        //     this.particles[500].mass = 0;
        // }
    }
}