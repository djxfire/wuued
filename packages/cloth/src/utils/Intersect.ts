import { Path, SvgOpt } from "~/core/Data";
export default class Intersect {
    private path: Path2D;
    private ctx: CanvasRenderingContext2D;
    constructor(shape: Path) {
        const { opts = [] } = shape;
        let pathStr = "";
        for (let i = 0; i < opts.length; i++) {
            const opt = opts[i];
            switch(opt.mtd) {
                case "M":
                case "L":
                    pathStr += `${opt.mtd} ${opt?.x} ${opt?.y}`;
                    break;
                case "H":
                    pathStr += `${opt.mtd} ${opt?.x}`;
                    break;
                case "V":
                    pathStr += `${opt.mtd} ${opt?.y}`;
                    break;
                case "C":
                    pathStr += `${opt.mtd} ${opt?.x1} ${opt?.y1} ${opt?.x2} ${opt?.y2} ${opt.x} ${opt.y}`;
                    break;
                case "S":
                case "Q":
                    pathStr += `${opt.mtd} ${opt?.x1} ${opt?.y1} ${opt.x} ${opt.y}`;
                    break;
                case "A0":
                    pathStr += `A ${opt.rx} ${opt.ry} 0 0 0 ${opt.x} ${opt.y}`;
                    break;
                case "A1":
                    pathStr += `A ${opt.rx} ${opt.ry} 0 0 1 ${opt.x} ${opt.y}`;
                    break;
                case "Z":
                    pathStr += 'Z';
                    break;
            }
        }
        const canvas = document.createElement("canvas");
        this.ctx = canvas.getContext("2d") as CanvasRenderingContext2D;
        this.path = new Path2D(pathStr);
    }

    public intersect(x: number, y: number) {
        return this.ctx.isPointInPath(this.path, x, y);
    }
}