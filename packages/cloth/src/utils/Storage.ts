export function get(key: string, defaultValue: any, json = true) {
  const value = localStorage.getItem(key);
  if (value) {
    if (json) return JSON.parse(value);
    return value;
  }
  return defaultValue;
}

export function set(key: string, value: any) {
  if (value) {
    if (typeof value === "object") {
      localStorage.setItem(key, JSON.stringify(value));
    } else {
      localStorage.setItem(key, value);
    }
  } else {
    localStorage.removeItem(key);
  }
}

export function getPure(key: string, defaultValue: any, json = true) {
  const value = sessionStorage.getItem(key);
  if (value) {
    if (json) return JSON.parse(value);
    return value;
  }
  return defaultValue;
}

export function setPure(key: string, value: any) {
  if (value) {
    if (typeof value === "object") {
      sessionStorage.setItem(key, JSON.stringify(value));
    } else {
      sessionStorage.setItem(key, value);
    }
  } else {
    sessionStorage.removeItem(key);
  }
}
