import Axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse } from "axios";

type ResponseType = "axios" | "service" | "data";

interface RequestOption {
  responseType: ResponseType;
}
let axios: AxiosInstance;
function getConfigedAxios() {
  if (!axios) {
    axios = Axios.create();
    axios.defaults.timeout = 10000;
    axios.interceptors.request.use(
      //TODO: 设置根据语言设置headers中的lang,登录状态设置authentication
      (config: AxiosRequestConfig) => config,
      (error) => Promise.reject(error)
    );
    axios.interceptors.response.use(
      (response: any) => Promise.resolve(response),
      (error) => Promise.reject(error)
    );
  }
  return axios;
}
export default async function request(
  req: AxiosRequestConfig,
  options?: RequestOption
) {
  const axiosInstance = getConfigedAxios();
  const response: AxiosResponse<{ msg: string; code: number; data: any }> =
    await axiosInstance(req);
  const responseType = options?.responseType || "data";
  console.log("response type===>", responseType, response);
  if (responseType === "service") {
    return response.data;
  } else if (responseType === "axios") {
    return response;
  }
  return response.data?.data;
}
