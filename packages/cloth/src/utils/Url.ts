export function ensureEndSlash(url: string) {
    return url.endsWith("/") ? url : `${url}/`;
}

export function pathJoin(...args: string[]) {
    return args.join("/").replace(/\/+/g, "/");
}

export function pathDirname(path: string) {
    if (path.endsWith('/')) {
        path = path.slice(0, -1);
    }
    const arr = path.split('/');
    arr.pop();
    return arr.join('/');
}

export function ensureProtocol(path: string) {
    if (!globalThis?.window) {
        return path.startsWith('http') ? path : `http:${path}`;
    }
    return path.startsWith('http') ? path : `${window.location.protocol}${path}`;
}