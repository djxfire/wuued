import { BufferGeometry, Float32BufferAttribute, Shape, Vector2, ShapeUtils, Vector3 } from "three";
import { Path } from "~/core/Data";
import Intersect from "~/utils/Intersect";
export interface ShapeOption {
    widthSegments: number;
    heightSegments: number;
    curveSegments: number;
}
export default class ShapeGeometry extends BufferGeometry {
    private uvArray: number[];
    private verticesArray: number[];
    private widthSegments: number;
    private heightSegments: number;
    private curveSegments: number;
    private indices: number[];
    private normals: number[];
    public shape: Shape;
    public intersect: Intersect;
    public points: { index: number; isIn: boolean }[][] = [];
    public xStep: number = 1;
    public yStep: number = 1;
    constructor(shape: Path, option: ShapeOption = {
        widthSegments: 10,
        heightSegments: 10,
        curveSegments: 12,
    }) {
        super();
        this.uvArray = [];
        this.verticesArray = [];
        this.indices = [];
        this.normals = [];
        this.widthSegments = option.widthSegments;
        this.heightSegments = option.heightSegments;
        this.curveSegments = option.curveSegments;
        this.shape = this.getShape(shape);
        this.intersect = new Intersect(shape);
        this.addShape()
        this.setIndex(this.indices);
        this.setAttribute("position", new Float32BufferAttribute(this.verticesArray, 3));
        this.setAttribute("uv", new Float32BufferAttribute(this.uvArray, 2));
        this.setAttribute("normal", new Float32BufferAttribute(this.normals, 3));
    }

    private getShape(path: Path): Shape {
        const shape = new Shape();
        const { opts } = path;
        for (let i = 0; i < opts.length; i++) {
            const opt = opts[i];
            switch(opt.mtd) {
                case "M":
                    shape.moveTo(opt.x, opt.y);
                    break;
                case "L":
                case "V":
                case "H":
                    shape.lineTo(opt.x, opt.y);
                    break;
                case "C":
                    shape.bezierCurveTo(
                        opt.x1 as number,
                        opt.y1 as number,
                        opt.x2 as number,
                        opt.y2 as number,
                        opt.x,
                        opt.y
                    );
                    break;
                case "S":
                    shape.bezierCurveTo(
                        opt.x1 as number,
                        opt.y1 as number,
                        opt.x1 as number,
                        opt.y1 as number,
                        opt.x,
                        opt.y
                    )
                    break;
                case "Q":
                    shape.quadraticCurveTo(
                        opt.x1 as number,
                        opt.y1 as number,
                        opt.x,
                        opt.y
                    );
                    break;
                case "T":
                    break;
                case "A0":
                    if (i > 0) {
                        const lastOpt = opts[i - 1];
                        const cx = (lastOpt.x + opt.x) / 2;
                        const cy = (lastOpt.y + opt.y) / 2;
                        shape.absarc(cx, cy, opt.rx as number, 0, Math.PI, true);
                    }
                    break;
                case "A1":
                    if (i > 0) {
                        const lastOpt = opts[i - 1];
                        const cx = (lastOpt.x + opt.x) / 2;
                        const cy = (lastOpt.y + opt.y) / 2;
                        shape.absarc(cx, cy, opt.rx as number, 0, Math.PI, false);
                    }
                    break;
                case "Z":
                    shape.closePath();
                    break;
            }
        }
        return shape;
    }

    private addShape() {
        let points = this.shape.getPoints(this.curveSegments);
        const reverse = ! ShapeUtils.isClockWise(points);
        if (reverse) {
            points = points.reverse();
        }
        if (points.length) {
            let max = points[0].clone();
            let min = points[0].clone();
            for (let i = 0; i < points.length; i++) {
                const point = points[i];
                if (point.x > max.x) {
                    max.x = point.x;
                }
                if (point.x < min.x) {
                    min.x = point.x;
                }
                if (point.y > max.y) {
                    max.y = point.y;
                }
                if (point.y < min.y) {
                    min.y = point.y;
                }
            }
            const xLen = max.x - min.x;
            const yLen = max.y - min.y;
            this.xStep = xLen / this.widthSegments;
            this.yStep = yLen / this.heightSegments;
            let index = 0;
            for (let i = 0; i < this.heightSegments; i++) {
                this.points[i] = [];
                for (let j = 0; j < this.widthSegments; j++) {
                    const x = j * this.xStep + min.x;
                    const y = i * this.yStep + min.y;
                    const isInPath = this.intersect.intersect(x, y);
                    if (isInPath) {
                        this.points[i].push({ index: index, isIn: true });
                        index += 1;
                        this.verticesArray.push(x, y, 0);
                    } else {
                        this.points[i].push({ index: -1, isIn: false });
                    }
                }
            }
            for (let i = 0; i < this.heightSegments - 1; i++) {
                for (let j = 0; j < this.widthSegments - 1; j++) {
                    if (this.points[i][j].isIn
                        && this.points[i + 1][j].isIn
                        && this.points[i + 1][j + 1].isIn
                    ) {
                        this.indices.push(
                            this.points[i][j].index,
                            this.points[i + 1][j + 1].index,
                            this.points[i + 1][j].index
                        );
                        this.normals.push(0, 0, 1);
                        this.normals.push(0, 0, 1);
                        this.normals.push(0, 0, 1);
                    }
                    if (this.points[i][j].isIn
                        && this.points[i][j + 1].isIn
                        && this.points[i + 1][j + 1].isIn
                    ) {
                        this.indices.push(
                            this.points[i][j].index,
                            this.points[i][j + 1].index,
                            this.points[i + 1][j + 1].index
                        );
                        this.normals.push(0, 0, 1);
                        this.normals.push(0, 0, 1);
                        this.normals.push(0, 0, 1);
                    }
                }
            }
        }
    }
}