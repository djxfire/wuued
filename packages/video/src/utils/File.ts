export async function getLocalFile(): Promise<File[]> {
    const fileInput = document.createElement("input");
    fileInput.type = "file";
    const event = new MouseEvent("click");
    fileInput.dispatchEvent(event);
    let resolveFn: null | Function = null;
    fileInput.onchange = () => {
      resolveFn && resolveFn(fileInput.files);
    };
    return new Promise((resolve) => {
      resolveFn = resolve;
    });
  }
  