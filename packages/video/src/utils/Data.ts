
export function updateData(target: any, path: string, value: any) {
    path.split('.').reduce((total: any, curVal: any, curIndex: number, arr: any[]) => {
        if (curIndex === arr.length - 1) {
            total[curVal] = value;
            
        }
        if (total[curVal] === undefined) {
            if (/\d/.test(curVal)) {
                total[arr[curIndex - 1]] = [];
            } else {
                total[curVal] = {};
            }
        }
        return total[curVal];
    } , target);
}