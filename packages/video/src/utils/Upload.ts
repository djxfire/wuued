import { upload } from "qiniu-js";
import axios from "axios";
import { v4 as uuidv4 } from "uuid";
interface IPutFileOption {
    xapi: string;
    rename?: string;
    scope?: boolean;
    type?: string;
}
async function getQiniuToken(scope: string | boolean = false) {
    const params: Record<string, any> = {};
    if (scope) {
        params.scope = scope;
    }
    const result = await axios.get("/api/upload/getToken");
    return result.data.data;
}

export async function putFile(
    file: File,
    options: IPutFileOption
) {
    const token = await getQiniuToken();
    console.log("file===>", file);
    const key = `${options.xapi}/${uuidv4()}`;
    const params = {
        fname: file.name,
    };
    const observable = upload(file, key, token, params, {});
    return new Promise((resolve, reject) => {
        observable.subscribe(
            null,
            (err) => {
                console.log("err===>", err);
                reject(err);
            },
            async (qiniuRes) => {
                console.log("qiniu res==>", qiniuRes);
                const allowedAction = qiniuRes.api;
                const uploadFileUrl = `${qiniuRes.host}${qiniuRes.key}`;
                resolve({
                    fsize: qiniuRes.fsize,
                    path: uploadFileUrl,
                    origin: qiniuRes.name,
                    filename: qiniuRes.name,
                });
            }
        )
    });
}