import { createApp, ref } from "vue";
import * as Storage from "~/utils/Storage";
import { isString } from "~/utils/Type";

interface LocaleRecord {
  [x: string]: Record<string, string | LocaleRecord>;
}

const isLocaleRecord = (v: string | LocaleRecord): v is LocaleRecord => {
  return !isString(v);
};
interface AppPlugin {
  options?: object;
  plugin: any;
}

function limit(
  url: string,
  options: { width: number; height?: number; quality?: number }
) {
  if (!url) {
    return "";
  }
  if (/imageView2/.test(url) || /\?/.test(url) || /\.svg$/.test(url)) {
    return url;
  }
  const ratio = window?.devicePixelRatio || 2;
  const { width, height, quality } = options;
  let finalUrl = `${url}?imageView2/2/w/${width * ratio}`;
  if (height) {
    finalUrl += `/h/${height * ratio}`;
  }
  if (/\.jpg|\.jpeg$/.test(url) && quality) {
    finalUrl += `/q/${quality}`;
  }
  finalUrl += "|imageslim";
  return finalUrl;
}
function _getUrlParams(url: string) {
  const urlStr = url.split("?")[1];
  let result: Record<string, any> = {};
  if (urlStr) {
    const paramsArr = urlStr.split("&");
    for (let i = 0; i < paramsArr.length; i++) {
      const arr = paramsArr[i].split("=");
      // 暂时不考虑数组
      result[arr[0]] = arr[1];
    }
  }
  return result;
}
export default class Application {
  private i18n: string;
  private theme: string;
  private name: string;
  private localeModules: LocaleRecord = {};
  private plugins: AppPlugin[] = [];
  private pageName: string = "";
  constructor(name: string) {
    const theme = Storage.get("global:theme", "default", false);
    const i18n = Storage.get("global:i18n", "en-US", false);
    this.name = name;
    this.theme = theme;
    this.i18n = i18n;

    this.initTheme();
    this.plugins = [];
    this.injectVue();

    this.getURLPath();
  }

  private getURLPath() {
    this.pageName = window.location.pathname.split("/").splice(1)?.[0];

    this.plugins.push({
      options: { that: this },
      plugin: {
        install(app: any, options: { that: Application }) {
          const pathnameArr = window.location.pathname.split("/");
          const pathArr = pathnameArr.splice(2);
          const params = _getUrlParams(window.location.href);
          const { that } = options;

          app.config.globalProperties.$route = ref({
            path: pathArr,
            params: params,
          });
          app.config.globalProperties.$route.push = (
            path: string,
            params?: any
          ) => {
            let href = `/${that.pageName}${path}`;

            if (params) {
              href +=
                "?" +
                Object.keys(params)
                  .map((vo) => `${vo}=${params[vo]}`)
                  .join("&");
            }
            window.history.replaceState(null, "", href);
            hashChangeFire();
          };

          const hashChangeFire = () => {
            const pathname = window.location.pathname;
            const pathArr = pathname.split("/").splice(2);
            const params = _getUrlParams(window.location.href);
            Object.assign(app.config.globalProperties.$route.value, {
              path: pathArr,
              params,
            });
          };
          const isHashChanged = () => {
            return false;
          };
          if ("onhashchange" in window) {
            window.addEventListener("hashchange", hashChangeFire);
          } else {
            setInterval(function () {
              const isChanged = isHashChanged();
              if (isChanged) {
                hashChangeFire();
              }
            }, 200);
          }
        },
      },
    });
  }

  private injectVue() {
    this.plugins.push({
      options: { that: this },
      plugin: {
        install(app: any, options: { that: Application }) {
          const { that } = options;
          app.config.globalProperties.$limit = limit;
        },
      },
    });
    this.plugins.push({
      options: { that: this },
      plugin: {
        install(app: any, options: { that: Application }) {
          const { that } = options;
          app.config.globalProperties.$t = (key: string, value?: string[]) => {
            let module = <LocaleRecord>that.localeModules[that.i18n];
            let i18Result: string | LocaleRecord = module;
            console.log(that.localeModules[that.i18n]);
            
            const keyArray = key.split(".");
            let backList = ["meta", "html"];
            for (let i = 0; i < keyArray.length; i++) {
              if (isLocaleRecord(i18Result)) {
                if (!i18Result && backList.includes(keyArray[0])) {
                  continue;
                }
                i18Result = i18Result[keyArray[i]] as string | LocaleRecord;
              }
            }
            if (!value) {
              return i18Result;
            } else {
              let result: string = "";
              value.forEach((v: string) => {
                result = (<string>i18Result).replace("%s", v);
              });

              return result;
            }
          };
        },
      },
    });
    this.plugins.push({
      options: { that: this },
      plugin: {
        install(app: any, options: { that: Application }) {
          const { that } = options;
          app.provide("$i18n", (key: string) => {
            that.i18n = key;
            Storage.set("global:i18n", key);
            location.reload();
          });
        },
      },
    });
    this.plugins.push({
      options: { that: this },
      plugin: {
        install(app, options: { that: Application }) {
          const { that } = options;
          app.provide("$theme", (key: string) => {
            that.theme = key;
            Storage.set("global:theme", key);
            that.initTheme();
          });
        },
      },
    });

  }

  private async initI18n() {
    const modules = import.meta.glob(`../locale/**/*.js`);

    for (const path in modules) {
      const result = path.match(
        new RegExp(`(global|${this.name})\/([^\/]+)\/([^\/]+).js$`)
      );
      if (result) {
        const i18n = result[2];
        const objKey = result[3];

        const v = (await (modules[path] as Function)()).default as any;
        if (!this.localeModules[i18n]) {
          this.localeModules[i18n] = {
            [objKey]: v,
          };
        } else {
          this.localeModules[i18n][objKey] = v;
        }
      }
    }
  }



  private initTheme() {
    document.body.className = this.theme;
  }

  use(plugin: Plugin, options?: object) {
    this.plugins.push({
      plugin,
      options,
    });
  }

  async start(vue: any, dom: string) {
    const app = createApp(vue, {});
    await this.initI18n();
    for (let i = 0; i < this.plugins.length; i++) {
      app.use(this.plugins[i].plugin, this.plugins[i].options);
    }
    app.mount(dom);
  }
}
