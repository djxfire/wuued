const scroll = {
    mounted(el: any, binding: any) {
      el.onscroll = function () {
        const { value } = binding;
        const { update } = value as { update: Function };
        const enable = JSON.parse(el.dataset.scroll || "false");
        const scrollH = el.scrollHeight;
        const scrollT = el.scrollTop;
        const { height } = el.getBoundingClientRect();
        if (scrollH - scrollT - height < 20 && enable) {
          update();
        }
      };
    },
  };
  export default scroll;
  