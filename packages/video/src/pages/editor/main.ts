import App from "./App.vue";
import Application from "~/core/App";
import "~/assets/iconfont/iconfont.js";
import "~/assets/iconfont/iconfont.css";

new Application("mockup").start(App, "#app");