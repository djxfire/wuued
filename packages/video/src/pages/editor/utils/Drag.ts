import Editor from "./editor/Editor";
export function enableDrag(ev: MouseEvent, editor: Editor): Promise<{x: number, y: number}> {
    ev.preventDefault();
    ev.stopPropagation();
    return new Promise((resolve) => {
        const target = (ev.currentTarget as HTMLElement).cloneNode(true) as HTMLElement;
        const image = (ev.currentTarget as HTMLElement);
        const { naturalWidth, naturalHeight } = (image as HTMLImageElement);
        let width = 100;
        let height = width / naturalWidth * naturalHeight;
        if (height > 100) {
            height = 100;
            width = height / naturalHeight * naturalWidth;
        }
        const { clientX, clientY } = ev;
        Object.assign(target.style, {
            position: "fixed",
            zIndex: 999,
            left: `${clientX - width / 2}px`,
            top: `${clientY - height / 2}px`,
            width: `${width}px`,
            height: `${height}px`,
            objectFit: "contain",
            border: `solid 1px #f2f3f4`,
        });
        document.body.appendChild(target);
        document.onmousemove = (e: MouseEvent) => {
            const { clientX, clientY } = e;
            Object.assign(target.style, {
                left: `${clientX - width / 2}px`,
                top: `${clientY - height / 2}px`,
            });
        };
        document.onmouseup = (e: MouseEvent) => {
            document.onmousemove = null;
            document.onmouseup = null;
            const { clientX, clientY } = e;
            const scale = editor.scale;
            const $editor = editor.$editor;
            const $container = editor.$container;
            const { left, top, width, height } = $editor.getBoundingClientRect();
            const {
                left: containerLeft,
                top: containerTop,
                width: containerWidth,
                height: containerHeight,
            } = $container.getBoundingClientRect();
            const tx = containerLeft + (containerWidth - width * scale) / 2;
            const ty = containerTop + (containerHeight - height * scale) / 2;
            const x = (clientX - tx) / scale;
            const y = (clientY - ty) / scale;
            resolve({ x: Math.round(x), y: Math.round(y) });
            document.body.removeChild(target);
        }
    });
    
}