const draggable = true;
let scale = 1;
let onMoving = null;
let onMoved = null;
let onBoxSelected = null;
let containerTarget = null;
function detect(rect1, rect2) {
    const [x11, y11, x12, y12] = rect1;
    const [x21, y21, x22, y22] = rect2;
    const minX = Math.max(x11, x21);
    const minY = Math.max(y11, y21);
    const maxX = Math.min(x12, x22);
    const maxY = Math.min(y12, y22);
    return minX < maxX && minY < maxY;
}
function calculateMvBounding(x, y) {
    if (containerTarget) {
        const { left, top } = containerTarget.getBoundingClientRect();
        x = left;
        y = top;
    }
    
    const mvs = document.querySelectorAll('.mv-active');
    let [minX, minY, maxX, maxY] = [Number.MAX_SAFE_INTEGER, Number.MAX_SAFE_INTEGER, -1, -1];
    for (let i = 0; i < mvs.length; i++) {
        const mv = mvs[i];
        const bounding = mv.getBoundingClientRect();
        let { left, top, width, height } = bounding;
        left = left * scale - x;
        top = top * scale - y;
        if (left < minX) {
            minX = left;
        }
        if (top < minY) {
            minY = top;
        }
        if (maxX < left + width * scale) {
            maxX = left + width * scale;
        }
        if (maxY < top + height * scale) {
            maxY = top + height * scale;
        }
    }
    return [minX, minY, maxX, maxY];
}

function getAlignLine(rect, boxes) {
    const [minX, minY, maxX, maxY] = rect;
    const topBoxes = [];
    const leftBoxes = [];
    const rightBoxes = [];
    const bottomBoxes = [];
    for (let i = 0; i < boxes.length; i++) {
        const box = boxes[i];
        let { transform, width, height } = box.style;
        const translate = transform.match(/translate\((-?\d+\.?\d*)px,\s*(-?\d+\.?\d*)px\)/);
        const left = parseFloat(translate[1]);
        const top = parseFloat(translate[2]);
        width = parseFloat(width);
        height = parseFloat(height);

        if (left + width < minX) {
            leftBoxes.push({
                left,
                top,
                width,
                height,
                box,
            });
        }
        if (left > maxX) {
            rightBoxes.push({
                left,
                top,
                width,
                height,
                box,
            });
        }
        if (top + height < minY) {
            topBoxes.push({
                left,
                top,
                width,
                height,
                box,
            });
        }
        if (top > maxY) {
            bottomBoxes.push({
                left,
                top,
                width,
                height,
                box,
            });
        }
    }
    leftBoxes.sort((a, b) => b.left - a.left);
    leftBoxes.forEach(vo => {
        const { left, top, width, height, box } = vo;
        const center = (minY + maxY) / 2;
        if (Math.abs(top - minY) < 2) {
            // 顶对齐
        }
        if (Math.abs(top + height - maxY) < 2) {
            // 底对齐
        }
        if (Math.abs(top + height / 2 - minY) < 2) {
            // 中心对齐顶
        }
        if (Math.abs(top + height / 2 - maxY) < 2) {
            // 中心对齐底
        }
        if (Math.abs(top + height - minY) < 2) {
            // 底对顶
        }
        if (Math.abs(top - center) < 2) {
            // 中心对顶
        }
        if (Math.abs(top + height / 2 - center) < 2) {
            // 中心对中心
        }
        if (Math.abs(top + height - center) < 2) {
            // 中心对底
        }
        if (Math.abs(top - maxY) < 2) {
            // 顶对底
        }
    });
}

function spanMouseUp(e) {
    e.stopPropagation();
    const mvs = document.querySelectorAll('.mv-active');
    const movings = [];
    for (let i = 0; i < mvs.length; i++) {
        const mv = mvs[i];
        let { transform, width, height } = mv.style;
        const translate = transform.match(/translate\((-?\d+\.?\d*)px,\s*(-?\d+\.?\d*)px\)/);
        const left = parseFloat(translate[1]);
        const top = parseFloat(translate[2]);
        width = parseFloat(width);
        height = parseFloat(height);
        const tag = mv.dataset.mvTag;
        if (tag) {
            movings.push({
                tag,
                x: left,
                y: top,
                w: width,
                h: height,
            });
        }
    }
    onMoved && onMoved(movings);
    document.onmousemove = null;
    document.onmouseup = null;
    document.onpointermove = null;
    document.onpointerup = null;
}
function getLeftTopPoint(mvs) {
    let [minX, minY, maxX, maxY] = [
        Number.MAX_SAFE_INTEGER, Number.MAX_SAFE_INTEGER,
        Number.MIN_SAFE_INTEGER, Number.MIN_SAFE_INTEGER,
    ];
    for (let i = 0; i < mvs.length; i++) {
        const mv = mvs[i];
        let { transform, width, height } = mv.style;
        const translate = transform.match(/translate\((-?\d+\.?\d*)px,\s*(-?\d+\.?\d*)px\)/);
        const left = parseFloat(translate[1]);
        const top = parseFloat(translate[2]);
        width = parseFloat(width);
        height = parseFloat(height);
        if (minX > left) {
            minX = left;
        }
        if (minY > top) {
            minY = top;
        }
        if (maxX < left + width) {
            maxX = left + width;
        }
        if (maxY < top + height) {
            maxY = top + height;
        }
    }
    return [minX, minY, maxX, maxY];
}
function getRectBounding(editor, mv, mvBox,[minX, minY], [distX, distY], ratio) {
    let { width, height, transform } = mv.style;
    const translate = transform.match(/translate\((-?\d+\.?\d*)px,\s*(-?\d+\.?\d*)px\)/);
    const left = parseFloat(translate[1]);
    const top = parseFloat(translate[2]);
    console.log("width==>", width);
    width = parseFloat(width);
    height = parseFloat(height);
    const relativeDistX = width * ratio;
    const relativeDistY = height * ratio;
    console.log("relative x==>", relativeDistX, relativeDistY, width, width * ratio);
    const mvBoxLeft = parseFloat(mvBox.style.left) + distX;
    const mvBoxTop = parseFloat(mvBox.style.top) + distY;
    const { left: editorLeft, top: editorTop } = editor.getBoundingClientRect();
    const { left: containerLeft, top: containerTop } = containerTarget.getBoundingClientRect();
    const mvLeft = ((mvBoxLeft + containerLeft) / scale - editorLeft);
    const mvTop = ((mvBoxTop + containerTop) / scale - editorTop);
    const w = width - relativeDistX;
    const h = height - relativeDistY;
    const sca = w / width;
    const x = (left - minX) * sca + mvLeft;
    const y = (top - minY) * sca + mvTop;
    return [x, y, w, h];
}
function getRectBoundingRatio(editor, mv, mvBox,[minX, minY], [distX, distY], [ratioX, ratioY]) {
    let { width, height, transform } = mv.style;
    const translate = transform.match(/translate\((-?\d+\.?\d*)px,\s*(-?\d+\.?\d*)px\)/);
    const left = parseFloat(translate[1]);
    const top = parseFloat(translate[2]);
    
    width = parseFloat(width);
    height = parseFloat(height);
    const relativeDistX = width * ratioX;
    const relativeDistY = height * ratioY;
    const mvBoxLeft = parseFloat(mvBox.style.left) + distX;
    const mvBoxTop = parseFloat(mvBox.style.top) + distY;
    const { left: editorLeft, top: editorTop } = editor.getBoundingClientRect();
    const { left: containerLeft, top: containerTop } = containerTarget.getBoundingClientRect();
    const mvLeft = ((mvBoxLeft + containerLeft) / scale - editorLeft);
    const mvTop = ((mvBoxTop + containerTop) / scale - editorTop);
    const w = width - relativeDistX;
    const h = height - relativeDistY;
    const sca = w / width;
    const x = (left - minX) * sca + mvLeft;
    const y = (top - minY) * sca + mvTop;
    return [x, y, w, h];
}
function getDistance(e, mvBox, [lastX, lastY]) {
    const { clientX, clientY } = e;
    let distX = clientX - lastX;
    let distY = clientY - lastY;
    const boxWidth = parseFloat(mvBox.style.width);
    const boxHeight = parseFloat(mvBox.style.height);
    let ratio = 1;
    if (Math.abs(distX / boxWidth) > Math.abs(distY / boxHeight)) {
        ratio = distX / boxWidth;
    } else {
        ratio = distY / boxHeight;
    }
    distX = ratio * boxWidth;
    distY = ratio * boxHeight;
    return [distX, distY, ratio];
}
function multiSelect(container, ele, editor) {
    containerTarget = container;
    const dom = container;
    const { left: x, top: y } = dom.getBoundingClientRect();
    let downX = -1;
    let downY = -1;
    dom.addEventListener('mousedown', (e) => {
        const { clientX, clientY } = e;
        downX = (clientX - x);
        downY = (clientY - y);
        let mvBox = document.querySelector('.movable-box');
        if (mvBox) {
            mvBox.parentNode.removeChild(mvBox);
        }
        const mvs = document.querySelectorAll('.mv-active');
        for (let i = 0; i < mvs.length; i++) {
            mvs[i].classList.remove('mv-active');
        }
        const path = (e.composedPath && e.composedPath()) || e.path
        const el = path.find(vo => vo.classList && vo.classList.contains(ele.replace('.', '')));
        if (el) {
            el.classList.add('mv-active');
        } else {
            document.onmousemove = (e) => {
                e.preventDefault();
                const { clientX, clientY } = e;
                const targetX = (clientX - x);
                const targetY = (clientY - y);
                const eles = document.querySelectorAll(ele);
                for (let i = 0; i < eles.length; i++) {
                    const el = eles[i];
                    const bounding = el.getBoundingClientRect();
                    const editorBounding = editor.getBoundingClientRect();
                    const { left, top, width, height } = bounding;
                    const rect1 = [downX, downY, targetX, targetY];
                    const rectX = left * scale - x;
                    const rectY = top * scale - y;
                    const rect2 = [rectX, rectY, (left + width) * scale - x, (top + height) * scale - y];
                    const isSel = detect(rect1, rect2);
                    console.log('is sel==>', isSel);
                    if (isSel && !el.classList.contains('mv-active')) {
                        el.classList.add('mv-active');
                    }
                }
                let containerBox = document.querySelector('.select-box');
                if (!containerBox) {
                    containerBox = document.createElement('div');
                    containerBox.classList.add('select-box');
                    container.appendChild(containerBox);
                }
                containerBox.style.left = `${Math.min(downX, targetX)}px`;
                containerBox.style.top = `${Math.min(downY, targetY)}px`;
                containerBox.style.width = `${Math.abs(targetX - downX)}px`;
                containerBox.style.height = `${Math.abs(targetY - downY)}px`;
            }
        }
        
        document.onmouseup = (e) => {
            const containerBox = document.querySelector('.select-box');
            if (containerBox) {
                container.removeChild(containerBox);
            }
            let [minX, minY, maxX, maxY] = calculateMvBounding(x, y);
            let mvBox = document.querySelector('.movable-box');
            if (!mvBox) {
                mvBox = document.createElement('div');
                mvBox.classList.add('movable-box');
                mvBox.style.position = 'absolute';
                container.appendChild(mvBox);
            }
            mvBox.style.left = `${minX}px`;
            mvBox.style.top =  `${minY}px`;
            mvBox.style.width = `${maxX - minX}px`;
            mvBox.style.height = `${maxY - minY}px`;
            mvBox.style.position = 'relative';
            const mva = document.querySelectorAll('.mv-active');
            const selectedMarks = [];
            for (let i = 0; i < mva.length; i++) {
                const mv = mva[i];
                const mvTag = mv.dataset.mvTag;
                selectedMarks.push(mvTag);
            }
            onBoxSelected && onBoxSelected(selectedMarks);
            mvBox.onmousedown = (e) => {
                e.stopPropagation();
                let { clientX: lastX, clientY: lastY } = e;
                document.onmousemove = (e) => {
                    e.preventDefault();
                    const { clientX, clientY } = e;
                    const mvs = document.querySelectorAll('.mv-active');
                    let distX = clientX - lastX;
                    let distY = clientY - lastY;
                    const movings = [];
                    let [minX, minY, maxX, maxY] = getLeftTopPoint(mvs);
                    for (let i = 0; i < mvs.length; i++) {
                        const mv = mvs[i];
                        const [x, y, w, h] = getRectBounding(
                            editor,
                            mv,
                            mvBox,
                            [minX, minY],
                            [distX, distY],
                            0
                        );
                        const tag = mv.dataset.mvTag;
                        if (tag) {
                            movings.push({
                                tag,
                                x,
                                y,
                                w,
                                h,
                            });
                        }
                    }
                    onMoving && onMoving(movings);
                    mvBox.style.left = `${parseFloat(mvBox.style.left) + distX}px`;
                    mvBox.style.top = `${parseFloat(mvBox.style.top) + distY}px`;
                    
                    const notActives = document.querySelectorAll(`${ele}:not(.mv-active)`);

                    console.log('not active===>', notActives);
                    
                    lastX = clientX;
                    lastY = clientY;
                }
                document.onmouseup = (e) => {
                    spanMouseUp(e);
                }
            }
            const leftTop = document.createElement('div');
            leftTop.style.display = 'inline-block';
            leftTop.style.width = '10px';
            leftTop.style.height = '10px';
            leftTop.style.border = 'solid 1px var(--primary-color)';
            leftTop.style.position = 'absolute';
            leftTop.style.left = '-5px';
            leftTop.style.top = '-5px';
            mvBox.appendChild(leftTop);
            leftTop.onmousedown = (e) => {
                e.stopPropagation();
                let { clientX: lastX, clientY: lastY } = e;
                document.onmousemove = (e) => {
                    e.preventDefault();
                    const { clientX, clientY } = e;
                    const mvs = document.querySelectorAll('.mv-active');
                    const [distX, distY, ratio] = getDistance(e, mvBox, [lastX, lastY]);
                    let canSmall = true;
                    const movings = [];
                    const promises = [];
                    let [minX, minY] = getLeftTopPoint(mvs);
                    for (let i = 0; i < mvs.length; i++) {
                        const mv = mvs[i];
                        const [x, y, w, h] = getRectBounding(
                            editor,
                            mv,
                            mvBox,
                            [minX, minY],
                            [distX, distY],
                            ratio
                        );
                        if (w < 0 || h < 0) {
                            canSmall = false;
                            break;
                        }
                        promises.push(new Promise((resolve) => resolve({
                            mv, x, y, w, h
                        })));
                    }
                    if (canSmall) {
                        Promise.all(promises.map(vo => vo.then(res => {
                            const { mv, x, y, w, h } = res;
                            mv.style.transform = `translate(${x}px, ${y}px)`;
                            mv.style.width = `${w}px`;
                            mv.style.height = `${h}px`;
                            const tag = mv.dataset.mvTag;
                            if (tag) {
                                movings.push({
                                    tag,
                                    x,
                                    y,
                                    w,
                                    h,
                                });
                            }
                            return Promise.resolve();
                        }))).then(() => {
                            onMoving && onMoving(movings);
                            mvBox.style.left = `${parseFloat(mvBox.style.left) + distX}px`;
                            mvBox.style.top = `${parseFloat(mvBox.style.top) + distY}px`;
                            mvBox.style.width = `${parseFloat(mvBox.style.width) - distX}px`;
                            mvBox.style.height = `${parseFloat(mvBox.style.height) - distY}px`;
                        });
                    }
                    lastX = clientX;
                    lastY = clientY;
                }
                document.onmouseup = (e) => {
                    spanMouseUp(e);
                }
            }
            
            const rightTop = document.createElement('div');
            rightTop.style.display = 'inline-block';
            rightTop.style.width = '10px';
            rightTop.style.height = '10px';
            rightTop.style.border = 'solid 1px var(--primary-color)';
            rightTop.style.position = 'absolute';
            rightTop.style.right = '-5px';
            rightTop.style.top = '-5px';
            mvBox.appendChild(rightTop);
            rightTop.onmousedown = (e) => {
                e.stopPropagation();
                let { clientX: lastX, clientY: lastY } = e;
                document.onmousemove = (e) => {
                    e.preventDefault();
                    const mvs = document.querySelectorAll('.mv-active');
                    const { clientX, clientY } = e;
                    let distX = clientX - lastX;
                    let distY = clientY - lastY;
                    const boxWidth = parseFloat(mvBox.style.width);
                    const boxHeight = parseFloat(mvBox.style.height);
                    let ratio = 1;
                    if (Math.abs(distX / boxWidth) > Math.abs(distY / boxHeight)) {
                        ratio = distX / boxWidth;
                        distX = ratio * boxWidth;
                        distY = -ratio * boxHeight;
                    } else {
                        ratio = -distY / boxHeight;
                        distX = ratio * boxWidth;
                        distY = -ratio * boxHeight;
                    }
                    let canSmall = true;
                    const movings = [];
                    const promises = [];
                    let [minX, minY] = getLeftTopPoint(mvs);
                    for (let i = 0; i < mvs.length; i++) {
                        const mv = mvs[i];
                        const [x, y, w, h] = getRectBounding(
                            editor,
                            mv,
                            mvBox,
                            [minX, minY],
                            [distX, distY],
                            -ratio
                        );
                        if (w < 0 || h < 0) {
                            canSmall = false;
                            break;
                        }
                        promises.push(new Promise((resolve) => resolve({
                            mv, x, y, w, h
                        })));
                    }
                    if (canSmall) {
                        Promise.all(promises.map(vo => vo.then(res => {
                            const { mv, x, y, w, h } = res;
                            mv.style.transform = `translate(${x}px, ${y}px)`;
                            mv.style.width = `${w}px`;
                            mv.style.height = `${h}px`;
                            const tag = mv.dataset.mvTag;
                            if (tag) {
                                movings.push({
                                    tag,
                                    x,
                                    y,
                                    w,
                                    h,
                                });
                            }
                            return Promise.resolve();
                        }))).then(() => {
                            onMoving && onMoving(movings);
                            mvBox.style.top = `${parseFloat(mvBox.style.top) + distY}px`;
                            mvBox.style.width = `${parseFloat(mvBox.style.width) + distX}px`;
                            mvBox.style.height = `${parseFloat(mvBox.style.height) - distY}px`;
                        });
                    }

                    lastX = clientX;
                    lastY = clientY;
                };
                document.onmouseup = (e) => {
                    spanMouseUp(e);
                };
            }
            
            const leftBottom = document.createElement('div');
            leftBottom.style.display = 'inline-block';
            leftBottom.style.width = '10px';
            leftBottom.style.height = '10px';
            leftBottom.style.border = 'solid 1px var(--primary-color)';
            leftBottom.style.position = 'absolute';
            leftBottom.style.left = '-5px';
            leftBottom.style.bottom = '-5px';
            mvBox.appendChild(leftBottom);
            leftBottom.onmousedown = (e) => {
                e.stopPropagation();
                let { clientX: lastX, clientY: lastY } = e;
                document.onmousemove = (e) => {
                    e.preventDefault();
                    const mvs = document.querySelectorAll('.mv-active');
                    const { clientX, clientY } = e;
                    let distX = clientX - lastX;
                    let distY = clientY - lastY;
                    const boxWidth = parseFloat(mvBox.style.width);
                    const boxHeight = parseFloat(mvBox.style.height);
                    let ratio = 1;
                    if (Math.abs(distX / boxWidth) > Math.abs(distY / boxHeight)) {
                        ratio = distX / boxWidth;
                        distX = ratio * boxWidth;
                        distY = -ratio * boxHeight;
                    } else {
                        ratio = -distY / boxHeight;
                        distX = ratio * boxWidth;
                        distY = -ratio * boxHeight;
                    }
                    let canSmall = true;
                    const movings = [];
                    const promises = [];
                    let [minX, minY] = getLeftTopPoint(mvs);
                    for (let i = 0; i < mvs.length; i++) {
                        const mv = mvs[i];
                        const [x, y, w, h] = getRectBounding(
                            editor,
                            mv,
                            mvBox,
                            [minX, minY],
                            [distX, distY],
                            ratio
                        );
                        if (w < 0 || h < 0) {
                            canSmall = false;
                            break;
                        }
                        promises.push(new Promise((resolve) => resolve({
                            mv, x, y, w, h
                        })));
                    }
                    if (canSmall) {
                        Promise.all(promises.map(vo => vo.then(res => {
                            const { mv, x, y, w, h } = res;
                            mv.style.transform = `translate(${x}px, ${y}px)`;
                            mv.style.width = `${w}px`;
                            mv.style.height = `${h}px`;
                            const tag = mv.dataset.mvTag;
                            if (tag) {
                                movings.push({
                                    tag,
                                    x,
                                    y,
                                    w,
                                    h,
                                });
                            }
                            return Promise.resolve();
                        }))).then(() => {
                            onMoving && onMoving(movings);
                            mvBox.style.left = `${parseFloat(mvBox.style.left) + distX}px`;
                            // mvBox.style.top = `${parseFloat(mvBox.style.top) + distY}px`;
                            mvBox.style.width = `${parseFloat(mvBox.style.width) - distX}px`;
                            mvBox.style.height = `${parseFloat(mvBox.style.height) + distY}px`;
                        });
                    }

                    lastX = clientX;
                    lastY = clientY;
                };
                document.onmouseup = (e) => {
                    spanMouseUp(e);
                };
            }
            
            const rightBottom = document.createElement('div');
            rightBottom.style.display = 'inline-block';
            rightBottom.style.width = '10px';
            rightBottom.style.height = '10px';
            rightBottom.style.border = 'solid 1px var(--primary-color)';
            rightBottom.style.position = 'absolute';
            rightBottom.style.right = '-5px';
            rightBottom.style.bottom = '-5px';
            mvBox.appendChild(rightBottom);
            rightBottom.onmousedown = (e) => {
                e.stopPropagation();
                let { clientX: lastX, clientY: lastY } = e;
                document.onmousemove = (e) => {
                    e.preventDefault();
                    const mvs = document.querySelectorAll('.mv-active');
                    const { clientX, clientY } = e;
                    let distX = clientX - lastX;
                    let distY = clientY - lastY;
                    const boxWidth = parseFloat(mvBox.style.width);
                    const boxHeight = parseFloat(mvBox.style.height);
                    let ratio = 1;
                    if (Math.abs(distX / boxWidth) > Math.abs(distY / boxHeight)) {
                        ratio = distX / boxWidth;                     
                    } else {
                        ratio = distY / boxHeight;
                    }
                    distX = ratio * boxWidth;
                    distY = ratio * boxHeight;
                    let canSmall = true;
                    const movings = [];
                    const promises = [];
                    let [minX, minY] = getLeftTopPoint(mvs);
                    for (let i = 0; i < mvs.length; i++) {
                        const mv = mvs[i];
                        const [x, y, w, h] = getRectBounding(
                            editor,
                            mv,
                            mvBox,
                            [minX, minY],
                            [distX, distY],
                            -ratio
                        );
                        if (w < 0 || h < 0) {
                            canSmall = false;
                            break;
                        }
                        promises.push(new Promise((resolve) => resolve({
                            mv, x, y, w, h
                        })));
                    }
                    if (canSmall) {
                        Promise.all(promises.map(vo => vo.then(res => {
                            const { mv, x, y, w, h } = res;
                            mv.style.transform = `translate(${x}px, ${y}px)`;
                            mv.style.width = `${w}px`;
                            mv.style.height = `${h}px`;
                            const tag = mv.dataset.mvTag;
                            if (tag) {
                                movings.push({
                                    tag,
                                    x,
                                    y,
                                    w,
                                    h,
                                });
                            }
                            return Promise.resolve();
                        }))).then(() => {
                            onMoving && onMoving(movings);
                            // mvBox.style.left = `${parseFloat(mvBox.style.left) + distX}px`;
                            // mvBox.style.top = `${parseFloat(mvBox.style.top) + distY}px`;
                            mvBox.style.width = `${parseFloat(mvBox.style.width) + distX}px`;
                            mvBox.style.height = `${parseFloat(mvBox.style.height) + distY}px`;
                        });
                    }
                    lastX = clientX;
                    lastY = clientY;
                };
                document.onmouseup = (e) => {
                    spanMouseUp(e);
                };
            }
            const mvs = document.querySelectorAll('.mv-active');
            console.log('mvs===>', mvs.length, mvs);
            
            if (mvs.length === 1) {
                const mv = mvs[0];
                const aspect = mv.dataset.mvAspect;
                if (aspect !== 'fit') {
                    const centerTop = document.createElement('div');
                    centerTop.style.position = 'absolute';
                    centerTop.style.left = 'calc(50% - 5px)';
                    centerTop.style.top = '-5px';
                    centerTop.style.display = 'inline-block';
                    centerTop.style.width = '10px';
                    centerTop.style.height = '10px';
                    centerTop.style.border = 'solid 1px var(--primary-color)';
                    centerTop.style.position = 'absolute';
                    mvBox.appendChild(centerTop);
                    centerTop.onmousedown = (e) => {
                        e.stopPropagation();
                        let { clientX: lastX, clientY: lastY } = e;
                        document.onmousemove = (e) => {
                            e.preventDefault();
                            const mvs = document.querySelectorAll('.mv-active');
                            const { clientX, clientY } = e;
                            let distY = clientY - lastY;
                            const boxWidth = parseFloat(mvBox.style.width);
                            const boxHeight = parseFloat(mvBox.style.height);
                            let ratio = distY / boxHeight;;
                            let canSmall = true;
                            const movings = [];
                            const promises = [];
                            let [minX, minY] = getLeftTopPoint(mvs);
                            for (let i = 0; i < mvs.length; i++) {
                                const mv = mvs[i];
                                const [x, y, w, h] = getRectBoundingRatio(
                                    editor,
                                    mv,
                                    mvBox,
                                    [minX, minY],
                                    [0, distY],
                                    [0, ratio]
                                );
                                if (w < 0 || h < 0) {
                                    canSmall = false;
                                    break;
                                }
                                promises.push(new Promise((resolve) => resolve({
                                    mv, x, y, w, h
                                })));
                            }
                            if (canSmall) {
                                Promise.all(promises.map(vo => vo.then(res => {
                                    const { mv, x, y, w, h } = res;
                                    mv.style.transform = `translate(${x}px, ${y}px)`;
                                    mv.style.width = `${w}px`;
                                    mv.style.height = `${h}px`;
                                    const tag = mv.dataset.mvTag;
                                    if (tag) {
                                        movings.push({
                                            tag,
                                            x,
                                            y,
                                            w,
                                            h,
                                        });
                                    }
                                    return Promise.resolve();
                                }))).then(() => {
                                    onMoving && onMoving(movings);
                                    // mvBox.style.left = `${parseFloat(mvBox.style.left) + distX}px`;
                                    mvBox.style.top = `${parseFloat(mvBox.style.top) + distY}px`;
                                    // mvBox.style.width = `${parseFloat(mvBox.style.width) + distX}px`;
                                    mvBox.style.height = `${parseFloat(mvBox.style.height) - distY}px`;
                                });
                            }
                            lastX = clientX;
                            lastY = clientY;
                        }
                        document.onmouseup = (e) => {
                            spanMouseUp(e);
                        }
                    }
                    const centerBottom = document.createElement('div');
                    centerBottom.style.position = 'absolute';
                    centerBottom.style.left = 'calc(50% - 5px)';
                    centerBottom.style.bottom = '-5px';
                    centerBottom.style.display = 'inline-block';
                    centerBottom.style.width = '10px';
                    centerBottom.style.height = '10px';
                    centerBottom.style.border = 'solid 1px var(--primary-color)';
                    centerBottom.style.position = 'absolute';
                    mvBox.appendChild(centerBottom);
                    centerBottom.onmousedown = (e) => {
                        e.stopPropagation();
                        let { clientX: lastX, clientY: lastY } = e;
                        document.onmousemove = (e) => {
                            e.preventDefault();
                            const mvs = document.querySelectorAll('.mv-active');
                            const { clientX, clientY } = e;
                            let distY = clientY - lastY;
                            const boxWidth = parseFloat(mvBox.style.width);
                            const boxHeight = parseFloat(mvBox.style.height);
                            let ratio = distY / boxHeight;;
                            let canSmall = true;
                            const movings = [];
                            const promises = [];
                            let [minX, minY] = getLeftTopPoint(mvs);
                            for (let i = 0; i < mvs.length; i++) {
                                const mv = mvs[i];
                                const [x, y, w, h] = getRectBoundingRatio(
                                    editor,
                                    mv,
                                    mvBox,
                                    [minX, minY],
                                    [0, distY],
                                    [0, -ratio]
                                );
                                if (w < 0 || h < 0) {
                                    canSmall = false;
                                    break;
                                }
                                promises.push(new Promise((resolve) => resolve({
                                    mv, x, y, w, h
                                })));
                            }
                            if (canSmall) {
                                Promise.all(promises.map(vo => vo.then(res => {
                                    const { mv, x, y, w, h } = res;
                                    mv.style.transform = `translate(${x}px, ${y}px)`;
                                    mv.style.width = `${w}px`;
                                    mv.style.height = `${h}px`;
                                    const tag = mv.dataset.mvTag;
                                    if (tag) {
                                        movings.push({
                                            tag,
                                            x,
                                            y,
                                            w,
                                            h,
                                        });
                                    }
                                    return Promise.resolve();
                                }))).then(() => {
                                    onMoving && onMoving(movings);
                                    // mvBox.style.left = `${parseFloat(mvBox.style.left) + distX}px`;
                                    // mvBox.style.top = `${parseFloat(mvBox.style.top) + distY}px`;
                                    // mvBox.style.width = `${parseFloat(mvBox.style.width) + distX}px`;
                                    mvBox.style.height = `${parseFloat(mvBox.style.height) + distY}px`;
                                });
                            }
                            lastX = clientX;
                            lastY = clientY;
                        }
                        document.onmouseup = (e) => {
                            spanMouseUp(e);
                        }
                    }
                    const leftCenter = document.createElement('div');
                    leftCenter.style.position = 'absolute';
                    leftCenter.style.top = 'calc(50% - 5px)';
                    leftCenter.style.left = '-5px';
                    leftCenter.style.display = 'inline-block';
                    leftCenter.style.width = '10px';
                    leftCenter.style.height = '10px';
                    leftCenter.style.border = 'solid 1px var(--primary-color)';
                    leftCenter.style.position = 'absolute';
                    mvBox.appendChild(leftCenter);
                    leftCenter.onmousedown = (e) => {
                        e.stopPropagation();
                        let { clientX: lastX, clientY: lastY } = e;
                        document.onmousemove = (e) => {
                            e.preventDefault();
                            const mvs = document.querySelectorAll('.mv-active');
                            const { clientX, clientY } = e;
                            let distY = clientY - lastY;
                            let distX = clientX - lastX;
                            const boxWidth = parseFloat(mvBox.style.width);
                            const boxHeight = parseFloat(mvBox.style.height);
                            let ratio = distX / boxWidth;;
                            let canSmall = true;
                            const movings = [];
                            const promises = [];
                            let [minX, minY] = getLeftTopPoint(mvs);
                            for (let i = 0; i < mvs.length; i++) {
                                const mv = mvs[i];
                                const [x, y, w, h] = getRectBoundingRatio(
                                    editor,
                                    mv,
                                    mvBox,
                                    [minX, minY],
                                    [distX, 0],
                                    [ratio, 0]
                                );
                                if (w < 0 || h < 0) {
                                    canSmall = false;
                                    break;
                                }
                                promises.push(new Promise((resolve) => resolve({
                                    mv, x, y, w, h
                                })));
                            }
                            if (canSmall) {
                                Promise.all(promises.map(vo => vo.then(res => {
                                    const { mv, x, y, w, h } = res;
                                    mv.style.transform = `translate(${x}px, ${y}px)`;
                                    mv.style.width = `${w}px`;
                                    mv.style.height = `${h}px`;
                                    const tag = mv.dataset.mvTag;
                                    if (tag) {
                                        movings.push({
                                            tag,
                                            x,
                                            y,
                                            w,
                                            h,
                                        });
                                    }
                                    return Promise.resolve();
                                }))).then(() => {
                                    onMoving && onMoving(movings);
                                    mvBox.style.left = `${parseFloat(mvBox.style.left) + distX}px`;
                                    // mvBox.style.top = `${parseFloat(mvBox.style.top) + distY}px`;
                                    mvBox.style.width = `${parseFloat(mvBox.style.width) - distX}px`;
                                    // mvBox.style.height = `${parseFloat(mvBox.style.height) + distY}px`;
                                });
                            }
                            lastX = clientX;
                            lastY = clientY;
                        }
                        document.onmouseup = (e) => {
                            spanMouseUp(e);
                        }
                    }
                    const rightCenter = document.createElement('div');
                    rightCenter.style.position = 'absolute';
                    rightCenter.style.top = 'calc(50% - 5px)';
                    rightCenter.style.right = '-5px';
                    rightCenter.style.display = 'inline-block';
                    rightCenter.style.width = '10px';
                    rightCenter.style.height = '10px';
                    rightCenter.style.border = 'solid 1px var(--primary-color)';
                    rightCenter.style.position = 'absolute';
                    mvBox.appendChild(rightCenter);
                    rightCenter.onmousedown = (e) => {
                        e.stopPropagation();
                        let { clientX: lastX, clientY: lastY } = e;
                        document.onmousemove = (e) => {
                            e.preventDefault();
                            const mvs = document.querySelectorAll('.mv-active');
                            const { clientX, clientY } = e;
                            let distY = clientY - lastY;
                            let distX = clientX - lastX;
                            const boxWidth = parseFloat(mvBox.style.width);
                            const boxHeight = parseFloat(mvBox.style.height);
                            let ratio = distX / boxWidth;;
                            let canSmall = true;
                            const movings = [];
                            const promises = [];
                            let [minX, minY] = getLeftTopPoint(mvs);
                            for (let i = 0; i < mvs.length; i++) {
                                const mv = mvs[i];
                                const [x, y, w, h] = getRectBoundingRatio(
                                    editor,
                                    mv,
                                    mvBox,
                                    [minX, minY],
                                    [distX, 0],
                                    [-ratio, 0]
                                );
                                if (w < 0 || h < 0) {
                                    canSmall = false;
                                    break;
                                }
                                promises.push(new Promise((resolve) => resolve({
                                    mv, x, y, w, h
                                })));
                            }
                            if (canSmall) {
                                Promise.all(promises.map(vo => vo.then(res => {
                                    const { mv, x, y, w, h } = res;
                                    mv.style.transform = `translate(${x}px, ${y}px)`;
                                    mv.style.width = `${w}px`;
                                    mv.style.height = `${h}px`;
                                    const tag = mv.dataset.mvTag;
                                    if (tag) {
                                        movings.push({
                                            tag,
                                            x,
                                            y,
                                            w,
                                            h,
                                        });
                                    }
                                    return Promise.resolve();
                                }))).then(() => {
                                    onMoving && onMoving(movings);
                                    // mvBox.style.left = `${parseFloat(mvBox.style.left) + distX}px`;
                                    // mvBox.style.top = `${parseFloat(mvBox.style.top) + distY}px`;
                                    mvBox.style.width = `${parseFloat(mvBox.style.width) + distX}px`;
                                    // mvBox.style.height = `${parseFloat(mvBox.style.height) + distY}px`;
                                });
                            }
                            lastX = clientX;
                            lastY = clientY;
                        }
                        document.onmouseup = (e) => {
                            spanMouseUp(e);
                        }
                    }
                }
            }
            
            document.onmousemove = null;
            document.onmouseup = null;
        }
    });
}

function setScale(v) {
    scale = v;
    let mvBox = document.querySelector('.movable-box');
    if (mvBox) {
        let [minX, minY, maxX, maxY] = calculateMvBounding();
        mvBox.style.left = `${minX}px`;
        mvBox.style.top =  `${minY}px`;
        mvBox.style.width = `${maxX - minX}px`;
        mvBox.style.height = `${maxY - minY}px`;
    }
}
function setOnMoving(call) {
    onMoving = call;
}
function setOnMoved(call) {
    onMoved = call;
}

function setOnBoxSelected(call) {
    onBoxSelected = call;
}

function removeMovableBox() {
    const mvBox = document.querySelector('.movable-box');
    if (mvBox) {
        mvBox.parentNode.removeChild(mvBox);
    }
    const mvs = document.querySelectorAll('.mv-active');
    for (let i = 0; i < mvs.length; i++) {
        mvs[i].classList.remove('mv-active');
    }
}

export {
    multiSelect,
    setScale,
    setOnMoving,
    setOnMoved,
    removeMovableBox,
    setOnBoxSelected,
};