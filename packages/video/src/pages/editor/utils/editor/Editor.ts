import RulerLine from "../rulerLine";
import {
    multiSelect, setScale, setOnMoved,
    setOnMoving, setOnBoxSelected,
} from '../draggable';
import { reactive } from "vue";
import { createApp } from "vue";
import App from "./App.vue";
import { updateData } from "~/utils/Data";
export interface EditorOption {
    width?: number;
    height?: number;
    ele: HTMLElement;
}
export interface ComponentData {
    name: string;
    mark: string;
    clip: any[];
    style: Record<string, any>;
    data: any,
    lock?: boolean;
}
export default class {
    public domElement: HTMLElement;
    public ruler: RulerLine;
    public $editor: HTMLElement;
    private width: number;
    private height: number;
    public scale: number = 1;
    public $container: HTMLElement;
    private components = reactive<ComponentData[]>([]);

    constructor(option: EditorOption) {
        this.domElement = document.createElement("div");
        this.$container = option.ele;
        this.$container.appendChild(this.domElement);
        Object.assign(this.domElement.style, {
            width: "100%",
            height: "100%",
            position: "relative",
            overflow: "hidden",
        });
        this.$editor = document.createElement("div");
        this.width = option.width ?? 1024;
        this.height = option.height ?? 576;
        Object.assign(this.$editor.style, {
            position: "absolute",
            left: 0,
            top: 0,
            bottom: 0,
            right: 0,
            margin: "auto",
            width: `${this.width}px`,
            height: `${this.height}px`,
            display: "inline-block",
        });
        this.domElement.appendChild(this.$editor);
        this.initScale(this.domElement, this.$editor);
        
        (this.$editor.style as any).zoom = this.scale;
        this.ruler = new RulerLine(this.domElement, this.$editor, { scale: this.scale });
        multiSelect(this.domElement, ".component-item-box", this.$editor);
        setScale(this.scale);
        this.initSelectMoving();
        this.initOnSelect();
        window.addEventListener("resize", () => this.ruler.render())
    }
    private initScale(container: HTMLElement, scene: HTMLElement) {
        const containerBounding = container.getBoundingClientRect();
        const sceneBounding = scene.getBoundingClientRect();
        const scaleX = containerBounding.width * 0.8 / sceneBounding.width;
        const scaleY = containerBounding.height * 0.8 / sceneBounding.height;
        const scale = Math.min(scaleX, scaleY);
        this.scale = scale;
        console.log("scale===>", this.scale);
    }
    private updateChanges(changes: any[]) {
        console.log("changes===>", changes);
        changes.forEach((vo) => {
            const { mark, changes = [] } = vo;
            const component = this.components.find((com) => com.mark === mark);
            changes.map(({ path, value }: { path: string, value: any }) => {
                updateData(component, path, value);
            });
        });
    }

    public setScale(val: number) {
        if (this.ruler) {
            this.ruler.scale = val;
            this.ruler.render();
        }
    }

    public render() {
        const app = createApp(App, {
            components: this.components,
        });
        app.mount(this.$editor);
        this.ruler.render();
    }

    private initOnSelect() {
        setOnBoxSelected((marks: string[]) => {

        });
    }
    public addComponent(component: ComponentData) {
        this.components.push(component);
    }
    private initSelectMoving() {
        setOnMoving((data: any[]) => {
            const changes = data.map((vo: any) => ({
                mark: vo.tag,
                changes: [{
                    path: 'style.left',
                    value: vo.x,
                }, {
                    path: 'style.top',
                    value: vo.y,
                }, {
                    path: 'style.width',
                    value: vo.w,
                }, {
                    path: 'style.height',
                    value: vo.h,
                }]
            }));
            let [minX, minY, maxX, maxY] = [
                Number.MAX_SAFE_INTEGER, Number.MAX_SAFE_INTEGER,
                Number.MIN_SAFE_INTEGER, Number.MIN_SAFE_INTEGER,
            ];
            console.log("data==>", data);
            data.forEach(vo => {
                const { x, y, w, h } = vo;
                if (x < minX) {
                    minX = x;
                }
                if (y < minY) {
                    minY = y;
                }
                if (maxX < x + w) {
                    maxX = x + w;
                }
                if (maxY < y + h) {
                    maxY = y + h;
                }
            });
            const height = maxY - minY;
            const width = maxX - minX;
            this.ruler.drawBox(minX, minY, width, height);
            this.updateChanges(changes);
        });
    }
}