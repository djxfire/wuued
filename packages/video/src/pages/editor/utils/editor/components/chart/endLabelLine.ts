export default (data: any[][], colors: string[]) => {
    const xData = data.map(vo => vo[0]).slice(1);
    const series = [];
    const seriesLen = data[0].length - 1;
    const legends = data[0].slice(1);
    const lineData = data.slice(1);

    for (let i = 0; i < seriesLen; i++) {
        series.push({
            name: legends[i],
            data: lineData.map(vo => vo[i + 1]),
            type: "line",
            showSymbol: false,
            color: colors[i % colors.length],
            endLabel: {
                show: true,
                formatter: function (params: { value: any, seriesName: string }) {
                  return params.seriesName + ': ' + params.value;
                }
            },
            labelLayout: {
                moveOverlap: "shiftY",
            },
            emphasis: {
                focus: "series"
            },
        });
    }
    const targetOption = {
        animation: true,
        animationDuration: 1000,
        grid: {
            // top: "32px",
            containLabel: true,
        },
        // legend: { data: legends, right: 0, },
        xAxis: {
            type: "category",
            data: xData,
            axisLine: {
                show: true,
            },
            axisLabel: {
                show: true,
            },
            splitLine: {
                show: false,
            }
        },
        yAxis: {
            type: "value",
            axisLabel: {
                show: true,
            },
            splitLine: {
                show: true,
            },
            axisLine: {
                show: true,
            },
            splitNumber: 3,
        },
        series,
    };
    return targetOption;
}