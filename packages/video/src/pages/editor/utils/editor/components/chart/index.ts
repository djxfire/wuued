import simpleLine from "./simpleLine";
import smoothLine from "./smoothLine";
import endLabelLine from "./endLabelLine";
import stackLine from "./stackLine";
import stepLine from "./stepLine";
import areaLine from "./areaLine";
import gradientLine from "./gradientLine";
import simpleBar from "./simpleBar";
import stackBar from "./stackBar";
import negativeBar from "./negativeBar";
import negativeStackBar from "./negativeStackBar";
import polarBar from "./polarBar";
import horiBar from "./horiBar";

export default {
    simpleLine,
    smoothLine,
    endLabelLine,
    stackLine,
    stepLine,
    areaLine,
    gradientLine,
    simpleBar,
    stackBar,
    negativeBar,
    negativeStackBar,
    polarBar,
    horiBar,
}