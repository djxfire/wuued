export default (data: any[][], colors: string[]) => {
    const xData = data.map(vo => vo[0]).slice(1);
    const series = [];
    const seriesLen = data[0].length - 1;
    const legends = data[0].slice(1);
    const lineData = data.slice(1);
    let max = Number.MIN_SAFE_INTEGER;
    for (let i = 0; i < seriesLen; i++) {
        let data = lineData.map(vo => vo[i + 1]);
        series.push({
            name: legends[i],
            data,
            coordinateSystem: "polar",
            type: "bar",
            showSymbol: false,
            color: colors[i % colors.length],
            label: {
                show: true,
                position: "middle",
                formatter: "{c}",
            }
        });
        let maxData = Math.max(...data);
        if (maxData > max) {
            max = maxData;
        }
    }
    const targetOption = {
        polar: {
            radius: [30, "80%"],
        },
        radiusAxis: {
            max,
        },
        legend: { data: legends, right: 0, },
        angleAxis: {
            type: "category",
            data: xData,
            startAngle: 0,
        },
        series,
    };
    return targetOption;
}