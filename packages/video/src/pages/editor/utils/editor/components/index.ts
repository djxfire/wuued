import XImage from "./Image.vue";
import XChart from "./Chart.vue";
export default {
    "x-image": XImage,
    "x-chart": XChart,
}