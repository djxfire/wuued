export default (data: any[][], colors: string[]) => {
    const xData = data.map(vo => vo[0]).slice(1);
    const series = [];
    const seriesLen = data[0].length - 1;
    const legends = data[0].slice(1);
    const lineData = data.slice(1);
    for (let i = 0; i < seriesLen; i++) {
        series.push({
            name: legends[i],
            data: lineData.map(vo => ({ value: vo[i + 1], label: { position: vo[i + 1] > 0 ? "left" : "right"} })),
            type: "bar",
            stack: "Total",
            showSymbol: false,
            color: colors[i % colors.length],
            emphasis: {
                focus: "series"
            },
        });
    }
    const targetOption = {
        animation: true,
        animationDuration: 1000,
        grid: {
            // top: "32px",
            containLabel: true,
        },
        legend: { data: legends, right: 0, },
        xAxis: {
            type: "value",
            position: "top",
            axisLine: {
                show: true,
            },
            axisLabel: {
                show: true,
            },
            splitLine: {
                show: true,
                lineStyle: {
                    type: "dashed",
                }
            }
        },
        yAxis: {
            type: "category",
            axisLabel: {
                show: true,
            },
            splitLine: {
                show: false,
            },
            axisLine: {
                show: false,
            },
            splitNumber: 3,
            data: xData,
        },
        series,
    };
    return targetOption;
}