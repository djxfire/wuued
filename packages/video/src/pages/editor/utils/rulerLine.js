
export default class {
    constructor(container, scene, option = { scale: 1 }) {
        this.container = container;
        this.scene = scene;
        this.scale = option.scale;
        
    }
    _draw_horizontal(container, scene, scale) {
        const leftWidth = (container - scene) / 2;
        const rightWidth = container - leftWidth;
        let step = 10;
        if (scale * step < 5) {
            step = 20
        }
        this.hStep = step;
        const leftNum = (leftWidth - 20) / scale / step;
        const rightNum = rightWidth / scale / step;
        let leftHtml = '';
        for (let i= 1; i < leftNum; i++) {
            leftHtml = `
            <line
                x1="${leftWidth - i * scale * step}"
                y1="0"
                x2="${leftWidth - i * scale * step}"
                y2="${i % 10 === 0 ? 16 : 8}"
                style="stroke: rgb(32, 32, 32); stroke-width:0.5"
            />
            ${
                i % 10 === 0 ? `
                    <text x="${leftWidth - i * scale * step + 2}" y="16" fill="rgb(32, 32, 32)" style="font-size:8px;">-${i * step}</text>
                ` : ''
            }
            ` + leftHtml;
        }
        let rightHtml = '';
        for (let i = 0; i < rightNum; i++) {
            rightHtml += `
            <line
                x1="${leftWidth + i * scale * step}"
                y1="0"
                x2="${leftWidth + i * scale * step}"
                y2="${i % 10 === 0 ? 16 : 8}"
                style="stroke: rgb(32, 32, 32); stroke-width:0.5"
            />
            ${
                i % 10 === 0 ? `
                    <text x="${leftWidth + i * scale * step + 2}" y="16" fill="rgb(32, 32, 32)" style="font-size:8px;">${i * step}</text>
                ` : ''
            }
            `;
        }
        return `
            <svg
                viewBox="0 0 ${container} 20"
                xmlns="http://www.w3.org/2000/svg"
                version="1.1"
                style="width: 100%;height: 100%;"
                preserveAspectRatio="xMinYMin meet"
            >
                ${leftHtml} ${rightHtml}
                <line x1="0" y1="20" x2="${container}" y2="20" style="stroke: rgb(32, 32, 32); stroke-width:0.5" />
            </svg>
        `;
    }
    _draw_vertical(container, scene, scale) {
        const topHeight = (container - scene) / 2;
        const bottomHeight = container - topHeight;
        let step = 10;
        if (scale * step < 5) {
            step = 20
        }
        this.vStep = step;
        const topNum = (topHeight - 20) / scale / step;
        const bottomNum = bottomHeight / scale / step;
        let topHtml = '';
        for (let i = 1; i < topNum; i++) {
            topHtml = `
                <line
                    x1="0"
                    y1="${topHeight - i * scale * step}"
                    x2="${i % 10 === 0 ? 16 : 8}"
                    y2="${topHeight - i * scale * step}"
                    style="stroke: rgb(32, 32, 32); stroke-width:0.5"
                />
                ${
                    i % 10 === 0 ? `
                        <text
                            y="${topHeight - i * scale * step + 2}"
                            x="16"
                            fill="rgb(32, 32, 32)"
                            writing-mode="tb"
                            style="font-size:8px;"
                        >
                        -${i * step}
                        </text>
                    ` : ''
                }
            ` + topHtml;
        }
        let bottomHtml = '';
        for (let i = 0; i < bottomNum; i++) {
            bottomHtml += `
                <line
                    x1="0"
                    y1="${topHeight + i * scale * step}"
                    x2="${i % 10 === 0 ? 16 : 8}"
                    y2="${topHeight + i * scale * step}"
                    style="stroke: rgb(32, 32, 32); stroke-width:0.5"
                />
                ${
                    i % 10 === 0 ? `
                        <text
                            y="${topHeight + i * scale * step + 2}"
                            x="16"
                            fill="rgb(32, 32, 32)"
                            writing-mode="tb"
                            style="font-size:8px;"
                        >
                        ${i * step}
                        </text>
                    ` : ''
                }
            `;
        }
        return `
        <svg
            viewBox="0 0 20 ${container}"
            xmlns="http://www.w3.org/2000/svg"
            version="1.1"
            style="width: 100%;height: 100%;"
            preserveAspectRatio="xMinYMin meet"
        >
            ${topHtml} ${bottomHtml}
            <line x1="20" y1="0" x2="20" y2="${container}" style="stroke: rgb(32, 32, 32); stroke-width:0.5" />
            <line x1="0" y1="20" x2="20" y2="20" style="stroke: rgb(32, 32, 32); stroke-width:0.5" />
        </svg>
        `;
    }
    _draw_top_box(container, scene, x, width) {
        const { hStep, scale } = this;
        const leftWidth = (container - scene) / 2;
        const xShift = x * scale;
        const widthShift = width * scale;
        return `
        <svg
            viewBox="0 0 ${container} 20"
            xmlns="http://www.w3.org/2000/svg"
            version="1.1"
            style="width: 100%;height: 100%;"
            preserveAspectRatio="xMinYMin meet"
        >
            <rect
                x="${leftWidth + xShift}"
                y="0"
                width="${widthShift}"
                style="fill:#4e72b8;fill-opacity:0.5;"
                height="20"
            />
        </svg>
        `;
    }
    _draw_left_box(container, scene, y, height) {
        const { vStep, scale } = this;
        const topHeight = (container - scene) / 2;
        const yShift = y * scale;
        const heightShift = height * scale;
        return `
        <svg
            viewBox="0 0 20 ${container}"
            xmlns="http://www.w3.org/2000/svg"
            version="1.1"
            style="width: 100%;height: 100%;"
            preserveAspectRatio="xMinYMin meet"
        >
            <rect
                y="${topHeight + yShift}"
                x="0"
                height="${heightShift}"
                style="fill:#4e72b8;fill-opacity:0.5;"
                width="20"
            />
        </svg>
        `;
    }
    drawBox(x, y, width, height) {
        const { container, scene, scale } = this;
        const doms = container.querySelectorAll('.rule-line-box');
        for (let i = 0; i < doms.length; i++) {
            container.removeChild(doms[i]);
        }
        const containerBox = container.getBoundingClientRect();
        const sceneBox = scene.getBoundingClientRect();
        const { width: containerWidth, height: containerHeight } = containerBox;
        const { width: sceneWidth, height: sceneHeight } = sceneBox;
        const hBox = document.createElement('div');
        const hSvg = this._draw_top_box(containerWidth, sceneWidth * scale, x, width);
        hBox.innerHTML = hSvg;
        hBox.classList = 'rule-line-box';
        hBox.style.position = 'absolute';
        hBox.style.zIndex = '10';  
        hBox.style.left = 0;
        hBox.style.top = 0;
        hBox.style.width = '100%';
        hBox.style.height = '20px';
        container.appendChild(hBox);
        const vBox = document.createElement('div');
        const vSvg = this._draw_left_box(containerHeight, sceneHeight * scale, y, height);
        vBox.innerHTML = vSvg;
        vBox.classList = 'rule-line-box';
        vBox.style.position = 'absolute';
        vBox.style.left = 0;
        vBox.style.top = 0;
        vBox.style.width = '20px';
        vBox.style.height = '100%';
        container.appendChild(vBox);
    }

    render() {
        const { container, scene, scale = 1 } = this;
        let horizontal = document.getElementById('horizontal-line');
        let vertical = document.getElementById('vertical-line');
        if (!horizontal) {
            horizontal = document.createElement('div');
            horizontal.id = 'horizontal-line';
            horizontal.style.position = 'absolute';      
            horizontal.style.left = 0;
            horizontal.style.top = 0;
            horizontal.style.width = '100%';
            horizontal.style.background = '#fff';
            horizontal.style.height = '20px';
            this.container.appendChild(horizontal);
        }
        if (!vertical) {
            vertical = document.createElement('div');
            vertical.id = 'vertical-line';
            vertical.style.position = 'absolute';
            vertical.style.left = 0;
            vertical.style.top = 0;
            vertical.style.height = '100%';
            vertical.style.background = '#fff';
            vertical.style.width = '20px';
            this.container.appendChild(vertical);
        }
        this.container.position = 'relative';
        const containerBox = container.getBoundingClientRect();
        const sceneBox = scene.getBoundingClientRect();
        const { width: containerWidth, height: containerHeight } = containerBox;
        const { width: sceneWidth, height: sceneHeight } = sceneBox; 
        const topSvg = this._draw_horizontal(containerWidth, sceneWidth * scale, scale);
        horizontal.innerHTML = topSvg;
        
        const leftSvg = this._draw_vertical(containerHeight, sceneHeight * scale, scale);
        vertical.innerHTML = leftSvg;
        
        
    }
}