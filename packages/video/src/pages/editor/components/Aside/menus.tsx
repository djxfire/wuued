import Scene from "./Scene/index.vue";
import Image from "./Image/index.vue";
import Video from "./Video/index.vue";
import Chart from "./Chart/index.vue";

export default [{
    icon: "icon-changjing",
    key: "scene",
    label: "场景",
    component: <Scene />,
}, {
    icon: "icon-tupiantianjia",
    key: "image",
    label: "图片",
    component: <Image />
}, {
    icon: "icon-shipin",
    key: "video",
    label: "视频",
    component: <Video />
}, {
    icon: "icon-tubiao",
    key: "chart",
    label: "图表",
    component: <Chart />,
}];