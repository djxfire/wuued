import Editor from "../utils/editor/Editor";
import { ref } from "vue";
let editor  = null;
let isInit = ref(false);

export function setEditor(e: Editor) {
    editor = e;
    isInit.value = true;
}

export function getEditor() {
    return editor;
}