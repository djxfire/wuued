import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import mpa from "@wuued/mpa";
import vueJsx from "@vitejs/plugin-vue-jsx";
import path from "path";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    
    mpa({
      scanDir: "src/pages",
    }),
    
    vue(),
    vueJsx(),
  ],
  resolve: {
    alias: {
      "@": path.resolve(__dirname, "scr/pages/"),
      "~": path.resolve(__dirname, "src/"),
    },
  },
  css: {
    preprocessorOptions: {
      less: {
        javascriptEnabled: true,
        modifyVars: {
          hack: `true;@import (reference) "${path.resolve(
            __dirname,
            "src/style/index.less"
          )}";`,
        },
      },
    },
  },
  server: {
    proxy: {
      "/api/": {
        changeOrigin: true,
        target: "http://127.0.0.1:7000/",
        wx: false,
        rewrite: (path: string) =>path.replace(/^\/api/, ""),
      },
    },
  },

})
